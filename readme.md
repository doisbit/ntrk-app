# React Native Boilerplate

### Prerequesites

### Package Modifiers

1. react-native-mauron85-background-geolocation

Open `node_modules/react-native-mauron85-background-geolocation/lib/build.gradle` and set `com.android.support:support-v4:23.+`

### Running AVD and Logcat

You can set something like these aliases to your shell (like zsh) in order to debug easier

`alias android="nohup /home/.../Android/Sdk/tools/emulator -avd [Nexus_5X_API_25] &`
and
`alias adblog="adb logcat | grep 'ReactNativeJS'"`

Where `[Nexus_5X_API_25]` is your Emulator identifier on AVD.

That way you can type "android" from anywhere to open the emulator and "adblog" to show only the log from ReactNativeJS

### 

## Background Geolocation