/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Alt from '../vendors/alt';
import _ from 'lodash';
import { Toast } from 'native-base';
import CommonsActions from '../actions/commons';

import i18n from '../services/i18n';

class CommonsStore {

    spinner: boolean;
    
    bindListeners: any;
    preventDefault: any;
    emitChange: any;

    constructor() {

        this.commonsStore_spinner = false;
        this.commonsStore_lastError = false;
        this.commonsStore_readedMessages = false;
        this.commonsStore_bootstrap = false;
        this.commonsStore_isUnreadedReaded = false;

        this.bindListeners({
            handleSpinner: CommonsActions.SPINNER,
            getReadedMessages: CommonsActions.GET_READED_MESSAGES,
            setReadedMessages: CommonsActions.SET_READED_MESSAGES,
            fetchReadMessages: CommonsActions.READ_MESSAGES,
            fetchError: CommonsActions.THROW_ERROR,
            getBootstrap: CommonsActions.GET_BOOTSTRAP,
            setBootstrap: CommonsActions.SET_BOOTSTRAP,
        });
    }

    handleSpinner( state: boolean ): void {
        this.commonsStore_spinner = state;
    }

    getReadedMessages() {
        this.commonsStore_readedMessages = false;
    }

    setReadedMessages(messages) {
        this.commonsStore_readedMessages = messages;
    }

    fetchReadMessages(state) {
        this.commonsStore_isUnreadedReaded = state;
    }

    getBootstrap() {
        this.commonsStore_bootstrap = {};
    }

    setBootstrap( bootstrap: Object ): void {
        this.commonsStore_bootstrap = bootstrap;
    }

    fetchError( error: any ): void {
        this.preventDefault();
        if(error.line) return console.error(error);
        let err = (typeof error == 'string' ? error : error.message);
        this.commonsStore_lastError = err;
        Toast.show({
            text: i18n.__(`errors.${err}`),
            position: 'bottom',
            buttonText: 'Ok'
        });
        this.emitChange();
    }

}

export default Alt.createStore( CommonsStore, 'CommonsStore' );