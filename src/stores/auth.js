/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Alt from '../vendors/alt';
import JWTDecode from 'jwt-decode';
import AuthActions from '../actions/auth';

class AuthStore {
    
    bindListeners: any;
    preventDefault: any;
    emitChange: any;
    resetValues: any;

    authStore_Auth: string;
    authStore_User: Object;
    authStore_HasStoredAuth: any;

    constructor() {
        
        this.authStore_Auth = "";
        this.authStore_User = { };
        this.authStore_HasStoredAuth = null;

        this.bindListeners({
            fetchSignup: AuthActions.FETCH_SIGNUP,
            fetchRefreshToken: AuthActions.REFRESH_TOKEN,
            fetchToken: AuthActions.FETCH_TOKEN,
            fetchFacebookLogin: AuthActions.FETCH_FACEBOOK_LOGIN,
            setToken: AuthActions.SET_TOKEN,
            handleLogout: AuthActions.LOGOUT,
            setLocalAuthStatus: AuthActions.UPDATE_LOCAL_AUTH_STATUS,
            fetchAuthWithStoredToken: AuthActions.AUTH_WITH_STORED_TOKEN,
        });
    }

    _resetValues(): void {
        this.authStore_Auth = "";
        this.authStore_User = { };
        this.authStore_HasStoredAuth = null;
    }
    
    /**
     * Fetch Facebook Login
     * 
     * Triggered when an user start the proccess
     * of authentication via facebook
     */
    fetchFacebookLogin(): void {
        this._resetValues();
    }

    fetchRefreshToken(): void {
        return;
    }

    /**
     * Fetch Signup
     * 
     * Triggered when an user start the proccess
     * of signup
     * 
     * @triggers this.handleToken
     */
    fetchSignup(): void {
        this._resetValues();
    }

    fetchAuthWithStoredToken(): void {
        this._resetValues();
    }

    /**
     * Fetch Token
     * 
     * Triggered when an user start the proccess of
     * signin
     * 
     */
    fetchToken(): void {
        this._resetValues();
    }

    fetchRefresh(): void {
        this._resetValues();
    }

    handleLogout(): void {
        this._resetValues();
    }

    /**
     * @param { boolean } status
     * @state authStore_HasStoredAuth 
     */
    setLocalAuthStatus( status: boolean): void {
        this.authStore_HasStoredAuth = status;
    }

    /**
     * Handle Token
     * 
     * Triggered after a local authentication succeeds.
     * Handles the token returned from local authentication
     * 
     * @param { object } auth The auth object returned from the authenticated user
     * @state authStore_Auth The JWT tokenken was returned from database, false otherwise
     * @state authStore_User The decoded JWT string that transcripts for the user info
     * @state authStore_HasStoredAuth If the auth has come from database or not
     */
    setToken( auth: object ): void {
        this.preventDefault();
        this.authStore_Auth = auth.token;
        if( auth.token.length ) this.authStore_User = JWTDecode( auth.token );
        this.authStore_HasStoredAuth = auth.fromDb;
        this.emitChange();
    }

}

export default Alt.createStore( AuthStore, 'AuthStore' );