/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import * as React from 'react'; 

import { NetInfo } from 'react-native';

import _ from 'lodash';

import AuthStore from '../stores/auth';
import AuthActions from '../actions/auth';

import CommonsStore from '../stores/commons';
import CommonsActions from '../actions/commons';

import Api from '../services/api';

import Config from '../services/config';

class ScreenComponent extends React.Component {

    _onChange: any;

    state: any = {
        ...this.getInitialState(),
        isConnected: true
    }

    constructor( props: Object ){
        super( props );
        
        this._onChange = this._onChange.bind( this );
        this._handleConnectionChange = this._handleConnectionChange.bind( this );

    }

    /**
     * Triggered when a connection is changed. True or False
     * 
     * @param { boolean } conStatus User is connected to the internet
     */
    _handleConnectionChange(conStatus) {
        this._onChange( { isConnected: conStatus } );
    }

    _onChange( state: any ) {

        /**
         * Triggered when user is not connected to the internet
         */
        if( _.has( state, 'isConnected' ) && !state.isConnected ){
            CommonsActions.throwError('NOT_CONNECTED');
        }
        
        /**
         * Sets into Api object the authStore_Auth token
         * in order to use authenticated methods from Api
         */
        if( _.has(state, 'authStore_Auth') &&
            !_.isEmpty( state.authStore_Auth ) &&
            _.isEmpty( Api.getJWT() )
        ){
            Api.setJWT( state.authStore_Auth );
        }

        /**
         * If:
         * 
         * 1. State contain lastError key
         * 2. lastError key is not empty
         * 
         * Clear the auth database and return user to Auth screen
         */
        if( _.has( state, 'commonsStore_lastError' ) &&
            [ 'WRNG_TKN' ].indexOf( state.commonsStore_lastError ) > -1
        ){
            return setImmediate(this.props.navigation.navigate.bind(
                this, 'Logout'
            ))
        }

        /**
         * If:
         * 
         * 1. authStore_Auth was set
         * 2. authStore_User has roleKey
         * 3. User is at 'Auth' or 'Signup' screen
         * 
         * Redirect user to ${FIRST_ROUTE} screen
         */
        if( _.has(state, 'authStore_User.id' ) && 
            [ 'Auth', 'Signup', 'Splash', 'Messages' ].indexOf( this.props.navigation.state.key ) > -1
        ){
            return setImmediate(this.props.navigation.navigate.bind(
                this, Config.get('FIRST_ROUTE')
            ))
        }

        /**
         * If:
         * 
         * 1. authStore_Auth exists but its empty
         * 2. User is currently on logout screen
         * 
         * Redirect user to Auth screen
         */
        if( (_.has(state, 'authStore_Auth') &&
            _.isEmpty( state.authStore_Auth ) &&
            this.props.navigation.state.key == 'Logout') ||
            state.authStore_HasStoredAuth === false
        ){
            return setImmediate(this.props.navigation.navigate.bind(
                this, 'Auth'
            ));
        }
        
        this.setState( _.assign( { }, state ) );

    }

    componentDidUpdate(prevProps, prevState){
        return;
    }

    componentDidMount(): void {
        AuthStore.listen( this._onChange );
        CommonsStore.listen( this._onChange );
        NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
    }
    
    componentWillUnmount(): void {
        AuthStore.unlisten( this._onChange );
        CommonsStore.unlisten( this._onChange );
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
    }

    getAuthStore(): any {
        return AuthStore;
    }

    getAuthActions(): any {
        return AuthActions;
    }

    getCommonsStore(): any {
        return CommonsStore;
    }

    getCommonsActions(): any {
        return CommonsActions;
    }

    getApi(): any {
        return Api;
    }

    getInitialState(): Object {
        return {
            ...AuthStore.getState(),
            ...CommonsStore.getState()
        }
    }

}

export default ScreenComponent;