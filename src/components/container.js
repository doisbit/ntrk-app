import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';

import { Container } from 'native-base';

import _ from 'lodash';

import CommonsStore from '../stores/commons';

import Style from '../services/styles';

import PropTypes from 'prop-types';

class NowTrackContainer extends Component {

    state: any = CommonsStore.getState();

    _onChange: void = this._onChange.bind( this );

    _onChange(state): void {
        this.setState( _.assign( { }, state ) );
    }

    componentDidMount(): void {
        CommonsStore.listen( this._onChange );
    }

    componentWillUnmount(): void {
        CommonsStore.unlisten( this._onChange );
    }

    render() {
        return (
            <Container style={this.props.style}>
                {this.state.commonsStore_spinner ? (
                    <ActivityIndicator style={Style.get('alignSelf.center flex')} size="large" color="white" />
                ) : 
                    this.props.children
                }
            </Container>
        );
    }

}

NowTrackContainer.propTypes = {
    style: PropTypes.object
}

export default NowTrackContainer;

