/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import React from 'react';
import _ from 'lodash';
import { Image, View, ImageBackground } from 'react-native';
import { Container, Content, Text, List, ListItem } from 'native-base';

import i18n from '../../services/i18n';

import ScreenComponent from '../screen';

type Props = {
    navigation: any
}

export default class SideBar extends ScreenComponent {

    state = super.getInitialState();

    constructor( props: any ) {
        super( props );
    }

    _onChange( state: Object ){
        this.setState( _.assign( { }, state ) );
    }

    componentDidMount() {
        super.componentDidMount();
    }

    componentWillUnmount() {
        super.componentWillUnmount()
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={{
                        height: 120,
                        alignSelf: 'stretch',
                    }}>
                        <ImageBackground
                            source={{
                                uri: 'https://wcrates.files.wordpress.com/2014/12/android-lollipop-material-wallpaper.png'
                            }}
                            style={{
                                alignSelf: 'stretch',
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: 120
                            }}
                        >
                        { this.state.authStore_User ? (
                            <Image
                                source={{
                                    uri: this.state.authStore_User.avatar
                                }}
                                style={{
                                    height: 80,
                                    width: 80,
                                    alignSelf: 'center',
                                }}
                            />
                        ) : null}
                        </ImageBackground>
                    </View>
                    <List>
                        <ListItem button
                            onPress={ () => { this.props.navigation.navigate('Logout')  }}>
                            <Text>
                                { _.capitalize(i18n.__('logout')) }
                            </Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}