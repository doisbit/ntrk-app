// @flow

import * as React from 'react';

import _ from 'lodash';

import { View } from 'react-native';
import { Container, Header, Left, Text, Body, Title, Right, Content } from 'native-base';

import ScreenComponent from '../../components/screen';
import Style from '../../services/styles';
import Config from '../../services/config';
import i18n from '../../services/i18n';

export default class DriverSettingsTab extends ScreenComponent {

    render() {
        return (
            <Container>
                <Header>
                    <Left style={Style.get('flex')}/>
                    <Body style={Style.get('flex')}>
                        <Title style={{alignSelf: 'center'}}>
                            { _.capitalize( i18n.__( 'settings' ) ) }
                        </Title>
                    </Body>
                    <Right style={Style.get('flex')} />
                </Header>
                <Content padder>
                    <Text>
                        Unlimited Power!
                    </Text>
                </Content>
            </Container>
        );
    }
}