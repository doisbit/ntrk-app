// @flow

import * as React from 'react';

import _ from 'lodash';

import { View, Image, ImageBackground, FlatList, Linking, Platform, SectionList } from 'react-native';
import moment from 'moment';
import { Container, Header, Left, Text, Body, Title, Right, Content, ListItem, Thumbnail, ActionSheet } from 'native-base';

import ScreenComponent from '../../components/screen';
import Utils from '../../services/utils';
import Style from '../../services/styles';
import Config from '../../services/config';
import i18n from '../../services/i18n';

import MapView from 'react-native-maps';

type Delivery = {
    deliveryId?: string, //"0888c7",
    clientId?: string, //"5c125277-dc82-4af4-b8dc-5e752dd8beeb",
    status?: number, //1,
    customerId?: string, //"cbdad9",
    customer?: Object,
    client?: Object,
    id?: string, //"094952",
    updatedAt?: string, //"2017-12-06T11:33:40.256Z",
    createdAt?: string, //"2017-12-06T11:33:40.256Z"
};

export default class DriverDeliveryTab extends ScreenComponent {

    _refreshDeliveries: any;
    _deliveryListRenderer: any;
    _showDeliveryOptions: any;
    _deliverySectionRenderer: any;

    state: Object = {
        ...super.getInitialState(),
        isFetching: false,
        deliveryChosen: false
    }

    constructor(props: any) {
        super(props);

        this._refreshDeliveries = this._refreshDeliveries.bind(this);
        this._deliveryListRenderer = this._deliveryListRenderer.bind(this);
        this._showDeliveryOptions = this._showDeliveryOptions.bind(this);
        this._deliverySectionRenderer = this._deliverySectionRenderer.bind(this);
    }

    componentDidMount() {
        super.componentDidMount();
        this._refreshDeliveries();
        super._startTracking();
        super._ensureToken();
        // super.getCommonsActions().spinner(false);
    }

    _refreshDeliveries() {
        super.getDeliveryActions().getDeliveries(super.getApi(), { status: 'not:5' }, this.state.authStore_User.driver);
    }

    _onChange(state){
        super._onChange(state);
    }

    // Action triggered when the driver clicks under a delivery
    async _showDeliveryOptions(delivery: Delivery) {
        if(this.state.deliveryChosen.id === delivery.id) {
            this.setState({deliveryChosen: false});
        }
        else{
            this.setState({deliveryChosen: delivery});
        }
        
        /*
        let nextStatus = _.find(this.DeliveryStatuses, { id: (delivery.status + 1) }).label;
        let options = [];
        let actionSheetCallback;
        let optionLabels = {
            nextStatus: `${_.capitalize(i18n.__('update_to'))}: ${nextStatus}`,
            openOnMaps: _.capitalize(i18n.__('open_on_maps')),
            reject: _.capitalize(i18n.__('reject')),
            sendWhats: _.capitalize(i18n.__('send_whats')),
            cancel: _.capitalize(i18n.__('cancel'))
        }

        // If there is a next status to go. Add this option as first.
        if (!_.isUndefined(nextStatus)) options.push(optionLabels.nextStatus);

        // Add the Open in Maps and Send Whatsapp options
        if (await Utils.canOpen('maps')) options.push(optionLabels.openOnMaps);
        if (await Utils.canOpen('whatsapp')) options.push(optionLabels.sendWhats);

        // In case the delivery was just created. Add the option for driver to reject it
        if (delivery.status == 1) options.push(optionLabels.reject);
        // Add the "cancel" option in order to close the Action Sheet
        options.push(optionLabels.cancel);

        // What happens when the user chooses an option
        actionSheetCallback = async (option, delivery) => {
            try {
                switch (option) {
                    // In case the driver chooses to change the status
                    case (optionLabels.nextStatus):
                        await super.getApi().put(Config.get('API.DELIVERY').replace(':id', delivery.id), {
                            status: delivery.status + 1
                        })
                        this._refreshDeliveries();
                        break;
                    // In case the driver chooses to open the delivery on maps
                    case (optionLabels.openOnMaps):
                        await Utils.openMap(delivery.customer.lat, delivery.customer.lng);
                        break;
                    case (optionLabels.sendWhats):
                        await Utils.sendWhats(delivery.customer.phone, '');
                }

            }
            catch (ERR) {
                super.getCommonsActions().throwError(ERR);
            }
        }

        ActionSheet.show(
            {
                options: options,
                cancelButtonIndex: (options.length - 1),
                destructiveButtonIndex: options.indexOf(optionLabels.reject) || false,
                title: delivery.customer.address
            },
            buttonIndex => {
                actionSheetCallback(options[buttonIndex], delivery);
            }
        )
        */

    }

    _deliveryListRenderer(delivery: Delivery) {

        return (
            <React.Fragment>
                <ListItem avatar onPress={this._showDeliveryOptions.bind(this, delivery.item)}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignSelf: 'stretch',
                    }}>
                        <Left>
                            <Thumbnail small square source={{ uri: delivery.item.client.logo }} />
                        </Left>
                        <Body>
                            <Text>{delivery.item.customer.address}</Text>
                            <Text note>{delivery.item.client.name}</Text>
                        </Body>
                        <Right>
                            <Text note>
                                {moment(delivery.item.createdAt).fromNow()}
                            </Text>
                            <Text note>
                                {Utils.distanceBetweenCoordinates(
                                    this.state.driverStore_Driver.lat,
                                    this.state.driverStore_Driver.lng,
                                    delivery.item.customer.lat,
                                    delivery.item.customer.lng
                                ).toFixed(2)}km
                            </Text>
                        </Right>
                    </View>
                </ListItem>
                {this.state.deliveryChosen.id == delivery.item.id ? (
                    <View style={{
                        height: 150,
                        flexDirection: 'row',
                        alignSelf: 'stretch',
                    }}>
                        <View style={{
                            flex: 2
                        }}>
                            <Button 
                                onPress={ this._handleSignin }
                                block
                                style={ Style.get(' background.accent border.square ') }>
                                <Text style={ Style.get(' color.white ') }>
                                    { i18n.__('login').toUpperCase() }
                                </Text>
                            </Button>
                        </View>
                        <MapView
                            style={{ flex: 1 }}
                            showsUserLocation={true}
                            minZoomLevel={15}
                            initialRegion={{
                                latitude: this.state.deliveryChosen.customer.lat,
                                longitude: this.state.deliveryChosen.customer.lng,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}
                        >
                            <MapView.Marker
                                coordinate={{latitude: this.state.deliveryChosen.customer.lat, longitude: this.state.deliveryChosen.customer.lng}}
                            />
                        </MapView>
                    </View>
                ) : null}
            </React.Fragment>
        );
    }

    _deliverySectionRenderer(data) {
        return (
            <ListItem itemDivider>
                <Text>{data.section.title}</Text>
            </ListItem>
        );
    }

    render() {

        return (
            <Container>
                <View style={{
                    flex: 1,
                    alignSelf: 'stretch',
                }}>
                    {/*this.state.driverStore_Driver.lat ? (
                        <MapView
                            style={{flex: 1}}
                            showsUserLocation={true}
                            minZoomLevel={10}
                            initialRegion={{
                                latitude: this.state.driverStore_Driver.lat,
                                longitude: this.state.driverStore_Driver.lng,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}
                        >
                            <MapView.Polyline
                                strokeWidth={4}
                                coordinates={this.state.deliveryStore_DeliveriesWaypoint}
                            />
                            {this.state.deliveryStore_Deliveries ? (
                                this.state.deliveryStore_Deliveries.map((d, i) => {
                                    return (
                                        <MapView.Marker
                                            key={i}
                                            coordinate={{latitude: d.customer.lat, longitude: d.customer.lng}}
                                        />
                                    )
                                })
                            ) : null}
                        </MapView>
                    ) : null*/}
                    <ImageBackground
                        source={{
                            uri: 'https://wcrates.files.wordpress.com/2014/12/android-lollipop-material-wallpaper.png'
                        }}
                        style={{
                            alignSelf: 'stretch',
                            justifyContent: 'center',
                            height: 120,
                            paddingLeft: 10,
                            paddingRight: 10
                        }}
                    >
                    { this.state.authStore_User ? (
                        <Image
                            source={{
                                uri: this.state.authStore_User.avatar
                            }}
                            style={{
                                alignSelf: 'center',
                                borderRadius: 75,
                                height: 80,
                                width: 80
                            }}
                        />
                    ) : null}
                    </ImageBackground>
                    <SectionList
                        onRefresh={this._refreshDeliveries}
                        refreshing={this.state.isFetching}
                        renderItem={this._deliveryListRenderer}
                        keyExtractor={(item, index) => `key-${index}`}
                        renderSectionHeader={this._deliverySectionRenderer}
                        sections={[
                            { data: _.filter(this.state.deliveryStore_Deliveries, (d) => { return [2, 3, 4].indexOf(d.status) > -1 }), title: _.capitalize(i18n.__('delivery_statuses.my_deliveries')) },
                            { data: _.filter(this.state.deliveryStore_Deliveries, { status: 1 }), title: _.capitalize(i18n.__('delivery_statuses.waiting_approval')) },
                        ]}
                    />
                </View>
            </Container>
        );
    }
}