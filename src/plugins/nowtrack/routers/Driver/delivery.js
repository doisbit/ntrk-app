// @flow

import * as React from 'react';

import _ from 'lodash';

import { View, FlatList, Linking, Platform, SectionList } from 'react-native';
import moment from 'moment';
import { Container, Header, Left, Text, Body, Title, Right, Content, ListItem, Thumbnail, ActionSheet } from 'native-base';

import ScreenComponent from '../../components/screen';
import Utils from '../../services/utils';
import Style from '../../services/styles';
import Config from '../../services/config';
import i18n from '../../services/i18n';

type Delivery = {
    deliveryId?: string, //"0888c7",
    clientId?: string, //"5c125277-dc82-4af4-b8dc-5e752dd8beeb",
    status?: number, //1,
    customerId?: string, //"cbdad9",
    customer?: Object,
    client?: Object,
    id?: string, //"094952",
    updatedAt?: string, //"2017-12-06T11:33:40.256Z",
    createdAt?: string, //"2017-12-06T11:33:40.256Z"
};

export default class DriverDeliveryTab extends ScreenComponent {

    _onRefreshList: any;
    _deliveryListRenderer: any;
    _showDeliveryOptions: any;
    _deliverySectionRenderer: any;

    constructor( props: any ){
        super( props );

        this.state = {
            ...super.getInitialState(),
            isFetching: false
        }

        this._onRefreshList = this._onRefreshList.bind( this );
        this._deliveryListRenderer = this._deliveryListRenderer.bind( this );
        this._showDeliveryOptions = this._showDeliveryOptions.bind( this );
        this._deliverySectionRenderer = this._deliverySectionRenderer.bind( this );
    }

    componentDidMount(){
        super.componentDidMount();
        this._onRefreshList();
        super._startTracking();
        super._ensureToken();
    }

    _onRefreshList(){
        super.getDeliveryActions().getDeliveries(super.getApi(), { status: 'not:5'});
    }

    // Action triggered when the driver clicks under a delivery
    async _showDeliveryOptions( delivery: Delivery ){
        let nextStatus = _.find(this.DeliveryStatuses, { id: ( delivery.status + 1 ) }).label;
        let options = [];
        let actionSheetCallback;
        let optionLabels = {
            nextStatus: `${ _.capitalize( i18n.__( 'update_to' ) )}: ${ nextStatus }`,
            openOnMaps: _.capitalize( i18n.__( 'open_on_maps' ) ),
            reject: _.capitalize( i18n.__( 'reject' ) ),
            sendWhats: _.capitalize( i18n.__( 'send_whats' ) ),
            cancel: _.capitalize( i18n.__( 'cancel' ) )
        }

        // If there is a next status to go. Add this option as first.
        if( !_.isUndefined( nextStatus ) ) options.push( optionLabels.nextStatus );
        
        // Add the Open in Maps and Send Whatsapp options
        if( await Utils.canOpen('maps') ) options.push( optionLabels.openOnMaps );
        if( await Utils.canOpen('whatsapp') ) options.push( optionLabels.sendWhats );

        // In case the delivery was just created. Add the option for driver to reject it
        if( delivery.status == 1 ) options.push( optionLabels.reject );
        // Add the "cancel" option in order to close the Action Sheet
        options.push( optionLabels.cancel );

        /**
         * What happens when the user chooses an option
         */
        actionSheetCallback = async (option, delivery) => {
            try{
                switch(option){
                    // In case the driver chooses to change the status
                case(optionLabels.nextStatus):
                    await super.getApi().put( Config.get( 'API.DELIVERY' ).replace( ':id', delivery.id ), {
                        status: delivery.status + 1
                    })
                    this._onRefreshList();
                    break;
                    // In case the driver chooses to open the delivery on maps
                case(optionLabels.openOnMaps):
                    await Utils.openMap( delivery.customer.lat, delivery.customer.lng );
                    break;
                case( optionLabels.sendWhats ):
                    await Utils.sendWhats( delivery.customer.phone, '' );
                }

            }
            catch( ERR ){
                super.getCommonsActions().throwError( ERR );
            }
        }

        ActionSheet.show(
            {
              options: options,
              cancelButtonIndex: (options.length - 1),
              destructiveButtonIndex: options.indexOf( optionLabels.reject ) || false,
              title: delivery.customer.address
            },
            buttonIndex => {
                actionSheetCallback(options[buttonIndex], delivery);
            }
          )

    }

    _deliveryListRenderer( delivery: Delivery ){

        return (
            <ListItem avatar onPress={ this._showDeliveryOptions.bind( this, delivery.item ) }>
              <Left>
                <Thumbnail small square source={ { uri: delivery.item.client.logo } } />
              </Left>
              <Body>
                <Text>{ delivery.item.customer.address }</Text>
                <Text note>{ delivery.item.client.name }</Text>
              </Body>
              <Right>
                <Text note>
                    { moment( delivery.item.createdAt ).fromNow() }
                </Text>
              </Right>
            </ListItem>
        );
    }

    _deliverySectionRenderer(data){
        return (
            <ListItem itemDivider>
                <Text>{data.section.title}</Text>
            </ListItem>
        );
    }

    render() {

        let delveriesOrganized =
            _.orderBy(
                this.state.deliveryStore_Deliveries,
                'createdAt'
            );

        return (
            <Container>
                <View style={ Style.get( 'flex' ) }>
                  <SectionList
                    onRefresh={this._onRefreshList}
                    refreshing={this.state.isFetching}
                    renderItem={this._deliveryListRenderer}
                    keyExtractor={(item, index) => `key-${index}`}
                    renderSectionHeader={this._deliverySectionRenderer}
                    sections={[
                        {data: _.filter(delveriesOrganized, {status: 4}), title: _.capitalize( i18n.__( 'delivery_statuses.arrived' ) ) },
                        {data: _.filter(delveriesOrganized, {status: 3}), title: _.capitalize( i18n.__( 'delivery_statuses.on_the_way' ) ) },
                        {data: _.filter(delveriesOrganized, {status: 2}), title: _.capitalize( i18n.__( 'delivery_statuses.assigned' ) ) },
                        {data: _.filter(delveriesOrganized, {status: 1}), title: _.capitalize( i18n.__( 'delivery_statuses.created' ) ) },
                    ]}
                    />
                </View>
            </Container>
        );
    }
}