// @flow

import * as React from 'react';
import Icon from '../../../../components/icon';
import { View, Image } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Carousel from 'react-native-carousel';
import { Button, Footer, FooterTab, Form, Item, Input, Label, Container, Header, Left, Text, Body, Title, Right, Content } from "native-base";
// import Firebase from '../../../../services/firebase';

import _ from 'lodash';

import Utils from '../../services/utils';

import NowTrackScreenComponent from '../../components/screen';
import i18n from '../../services/i18n';
import Config from '../../services/config';
import Style from '../../services/styles';

import Api from '../../../../services/api';

export default class LandingScreen extends NowTrackScreenComponent {

    static navigationOptions = {
        drawerLockMode: 'locked-closed',
    };

    _createDriver: any;
    _createClient: any;
    _triggerClientForm: any;

    constructor(props: any) {
        super(props);
        this._createDriver = this._createDriver.bind(this);
        this._createClient = this._createClient.bind(this);
        this._triggerClientForm = this._triggerClientForm.bind(this);

        this.state = {
            ...super.getInitialState(),
            client_form: false,
        }

    }

    // Creates a driver from authenticated user
    _createDriver(): void {
        super.getDriverActions().create(super.getApi(), Utils.getDeviceInfo());
    }

    // Trigger the client form
    _triggerClientForm(): void {
        this.setState({ client_form: true })
    }

    _createClient(): void {
        console.log(  );
    }

    render() {
        if (this.state.client_form) {
            return (
                <View style={Style.get(' flex ')}>
                    <Container style={{padding: 15}}>
                        <Content>
                            <Text style={{ marginBottom: 50, paddingLeft: 10, ...Style.get(' font.xl push_left ') }}>
                                Your info
                            </Text>
                            <Grid style={ Style.get(' grid.centered ') }>
                                <Col>
                                    <Form style={{paddingRight: 15}}>
                                        <Row>
                                            <Col>
                                                <Item stackedLabel>
                                                    <Label>{i18n.__('client_form.name')}</Label>
                                                    <Input ref="client.name" />
                                                </Item>
                                            </Col>
                                            <Col>
                                                <Item stackedLabel>
                                                    <Label>{i18n.__('client_form.phone')}</Label>
                                                    <Input ref="client.phone" />
                                                </Item>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Item stackedLabel>
                                                <Label>{i18n.__('client_form.document')}</Label>
                                                <Input ref="client.document" />
                                            </Item>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Item stackedLabel>
                                                    <Label>{i18n.__('client_form.zipcode')}</Label>
                                                    <Input ref="client.zipcode" />
                                                </Item>
                                            </Col>
                                            <Col>
                                                <Item stackedLabel>
                                                    <Label>{i18n.__('client_form.address')}</Label>
                                                    <Input ref="client.address" />
                                                </Item>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Item stackedLabel>
                                                    <Label>{i18n.__('client_form.city')}</Label>
                                                    <Input ref="client.city" />
                                                </Item>
                                            </Col>
                                            <Col>
                                                <Item stackedLabel>
                                                    <Label>{i18n.__('client_form.state')}</Label>
                                                    <Input ref="client.state" />
                                                </Item>
                                            </Col>
                                        </Row>
                                    </Form>                                        
                                    <Button onPress={this._createClient} block success style={{marginTop: 30}}>
                                        <Text>{i18n.__('create')}</Text>
                                    </Button>
                                </Col>                                    
                            </Grid>
                        </Content>
                    </Container>
                </View>
            )
        } else {
            return (
                <View style={Style.get(' flex ')}>
                    <Carousel
                        indicatorAtBottom={false}
                        indicatorColor={Style.colors.accent}
                        indicatorOffset={Utils.getScreenDimensions().height - 80}
                        animate={false}>
                        <View style={Style.get(' landing.pane ')}>
                            <Text style={{ ...Style.get(' font.xl push_left color.white ') }}>
                                Lorem ipsum!
                                    </Text>
                            <Text style={{ ...Style.get(' font.large push_left color.white ') }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Donec facilisis vulputate enim, a molestie tortor.
                                Donec a malesuada est. Proin suscipit suscipit augue quis
                                porttitor.
                            </Text>
                            <Image
                                style={{ width: 200, height: 160 }}
                                source={require('../../assets/img/landing/01.gif')} />
                        </View>
                        <View style={Style.get(' landing.pane ')}>
                            <Text style={{ ...Style.get(' font.xl push_left color.white ') }}>
                                Lorem ipsum!
                            </Text>
                            <Text style={{ ...Style.get(' font.large push_left color.white ') }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Donec facilisis vulputate enim, a molestie tortor.
                                Donec a malesuada est. Proin suscipit suscipit augue quis
                                porttitor.
                            </Text>
                            <Image
                                style={{ width: 200, height: 160 }}
                                source={require('../../assets/img/landing/01.gif')} />
                        </View>
                        <View style={Style.get(' landing.pane vertical_center')}>
                            <Text style={{ ...Style.get(' font.xl color.white margin.bottom._30') }}>
                                {_.capitalize(i18n.__('you_are_a'))}
                            </Text>
                            <View style={{ ...Style.get('flex_row margin.top._30 push_center full_width justify.space_around'), marginHorizontal: 20 }}>
                                <Button onPress={this._createDriver} style={Style.get('background.accent flex_col btn.square.lg')}>
                                    <Icon style={Style.get('margin.bottom._10')} name="motorcycle" size={35} color={Style.colors.white} />
                                    <Text>
                                        {_.capitalize(i18n.__('driver'))}
                                    </Text>
                                </Button>
                                <Button onPress={this._triggerClientForm} style={Style.get('background.accent flex_col btn.square.lg')}>
                                    <Icon style={Style.get('margin.bottom._10')} name="building" size={35} color={Style.colors.white} />
                                    <Text>
                                        {_.capitalize(i18n.__('commerce'))}
                                    </Text>
                                </Button>
                            </View>
                        </View>
                    </Carousel>
                </View>
            );
        }
    }
}