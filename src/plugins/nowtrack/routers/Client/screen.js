
import * as React from 'react';
import _ from 'lodash';

import { TabNavigator } from 'react-navigation';

import Icon from '../../../../components/icon';

import { Button, Text, Footer, FooterTab } from "native-base";

import ClientDeliveryTab from './delivery';
import ClientSettingsTab from './settings';

import Style from '../../services/styles';
import i18n from '../../services/i18n';


export default TabNavigator(
    {
        ClientDeliveryTab: { screen: ClientDeliveryTab },
        ClientSettingsTab: { screen: ClientSettingsTab },
    },
    {
        tabBarPosition: 'bottom',
        tabBarComponent: props => {

            return (
                <Footer>
                    <FooterTab>
                        <Button
                            vertical
                            active={ props.navigationState.index === 0 }
                            onPress={ () => props.navigation.navigate('DriverDeliveryTab') }>
                            <Icon name="paper-plane" size={ 20 } color={Style.colors.get('footer_icon')}/>
                            <Text style={ Style.get(' margin.top._5 ') }>
                                { _.capitalize(i18n.__('maracas')) }
                            </Text>
                        </Button>
                        <Button
                            vertical
                            active={ props.navigationState.index === 1 }
                            onPress={ () => props.navigation.navigate('DriverSettingsTab') }>
                            <Icon name="cogs" size={ 20 } color={Style.colors.get('footer_icon')}/>
                            <Text style={ Style.get(' margin.top._5 ') }>
                                { _.capitalize(i18n.__('settings')) }
                            </Text>
                        </Button>
                    </FooterTab>
                </Footer>
            );
        }
    }
);