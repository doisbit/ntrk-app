// @flow

import * as React from 'react';

import _ from 'lodash';

import { View } from 'react-native';
import { Container, Header, Left, Text, Body, Title, Right, Content } from 'native-base';

import ScreenComponent from '../../components/screen';
import Style from '../../services/styles';
import Config from '../../services/config';
import i18n from '../../services/i18n';

export default class ClientDeliveryTab extends ScreenComponent {

    render() {
        return (
            <Container>
                <Header>
                    <Left style={Style.get('flex')}/>
                    <Body style={Style.get('flex')}>
                        <Title style={{alignSelf: 'center'}}>
                            { _.capitalize( i18n.__( 'deliveries' ) ) }
                        </Title>
                    </Body>
                    <Right style={Style.get('flex')} />
                </Header>
                <Content padder>
                    <Text>Sup!</Text>
                </Content>
            </Container>
        );
    }
}