/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Alt from '../../../vendors/alt';
import _ from 'lodash';
import { Toast } from 'native-base';

import DriverActions from '../actions/driver';
import CommonsActions from '../../../actions/commons';

type Driver = {
    id?: string, // "d999e1"
    status?: number, // 1
    phone?: string, // "48991674935"
    device_type?: string, // "iPhone"
    lat?: string, // -25.12345
    lng?: string, // -32.91923
    device_version?: string, // "11.01"
    userId?: string, // "80e28cd4-78d0-46fa-91f7-9635dfba58fa"
    updatedAt?: string, // "2017-12-06T11:43:02.940Z"
    createdAt?: string // "2017-12-06T11:43:02.940Z"
}

/**
 * DriverStore
 * 
 * Store related to the driver
 * 
 * @author Alexandre Moraes
 */
class DriverStore {

    driverStore_Driver: Driver;
    bindListeners: any;

    constructor() {

        this.driverStore_Driver = {};

        this.bindListeners({
            fetchCreate: DriverActions.CREATE,
            fetchUpdate: DriverActions.UPDATE,
            fetchFind: DriverActions.FIND,
            setDriver: DriverActions.SET_DRIVER,
            setLocation: DriverActions.SET_LOCATION
        });
    }
    
    fetchUpdate(): void {
        return;
    }

    fetchFind(): void {
        this.driverStore_Driver = {};
    }

    fetchCreate(): void {
        this.driverStore_Driver = {};
    }

    setLocation( location: array ): void {
        this.driverStore_Driver = _.merge( this.driverStore_Driver, { lat: location[0], lng: location[1] } );
    }

    setDriver( driver: Driver ): void {
        this.driverStore_Driver = driver;
    }

}

export default Alt.createStore( DriverStore, 'DriverStore' );