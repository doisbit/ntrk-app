/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Alt from '../../../vendors/alt';
import _ from 'lodash';
import { Toast } from 'native-base';

import DeliveryActions from '../actions/delivery';
import CommonsActions from '../../../actions/commons';

type Delivery = {
    deliveryId?: string, //"0888c7",
    clientId?: string, //"5c125277-dc82-4af4-b8dc-5e752dd8beeb",
    status?: number, //1,
    customerId?: string, //"cbdad9",
    id?: string, //"094952",
    updatedAt?: string, //"2017-12-06T11:33:40.256Z",
    createdAt?: string, //"2017-12-06T11:33:40.256Z"
};

/**
 * DeliveryStore
 * 
 * Store related to the delivery
 * 
 * @author Alexandre Moraes
 */
class DeliveryStore {

    deliveryStore_Delivery: Delivery;
    deliveryStore_Deliveries: Array<Delivery>;
    bindListeners: any;

    constructor() {

        this.deliveryStore_Delivery = {};
        this.deliveryStore_Deliveries = [];
        this.deliveryStore_DeliveriesWaypoint = [];

        this.bindListeners({
            fetchCreateDelivery: DeliveryActions.CREATE_DELIVERY,
            fetchDeliveries: DeliveryActions.GET_DELIVERIES,
            setDelivery: DeliveryActions.SET_DELIVERY,
            setDeliveries: DeliveryActions.SET_DELIVERIES,
            getDeliveriesWaypoint: DeliveryActions.GET_DELIVERIES_WAYPOINT,
            setDeliveriesWaypoint: DeliveryActions.SET_DELIVERIES_WAYPOINT
        });
    }
    
    fetchDeliveries(): void {
        this.deliveryStore_Deliveries = [];
    }
    
    fetchCreateDelivery(): void {
        this.deliveryStore_Delivery = {};
    }

    getDeliveriesWaypoint() {
        this.deliveryStore_DeliveriesWaypoint = [];
    }

    setDelivery( delivery: Delivery ): void {
        this.deliveryStore_Delivery = delivery;
    }

    setDeliveriesWaypoint( waypoint ){
        this.deliveryStore_DeliveriesWaypoint = waypoint;
    }

    setDeliveries( deliveries: Array<Delivery> ): void {
        this.deliveryStore_Deliveries = deliveries;
    }

}

export default Alt.createStore( DeliveryStore, 'DeliveryStore' );