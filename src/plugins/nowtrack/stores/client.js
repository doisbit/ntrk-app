/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Alt from '../../../vendors/alt';
import _ from 'lodash';
import { Toast } from 'native-base';

import ClientActions from '../actions/client';
import CommonsActions from '../../../actions/commons';

type Client = {
    id?: string, //"005c3cf4-f231-4c81-b071-583737dfa05c",
    name?: string, //"Kikoni",
    document?: string, //"26896836000101",
    address?: string, //"Rua Rafael Bandeira, 328",
    phone?: string, //"40429662",
    zipcode?: string, //"88056125",
    city?: string, //"Florianópolis",
    state?: string, //"Santa Catarina",
    userId?: string, //"256652ea-07a3-44ef-90a5-3bf5c548431f",
    status?: number, //1,
    updatedAt?: string, //"2017-12-05T21:31:45.681Z",
    createdAt?: string, //"2017-12-05T21:31:45.681Z"
}

/**
 * ClientStore
 * 
 * Store related to the client
 * 
 * @author Alexandre Moraes
 */
class ClientStore {

    clientStore_Client: Client;
    bindListeners: any;

    constructor() {

        this.clientStore_Client = {};

        this.bindListeners({
            fetchCreate: ClientActions.CREATE,
            fetchUpdate: ClientActions.UPDATE,
            fetchFind: ClientActions.FIND,
            setClient: ClientActions.SET_CLIENT,
        });
    }
    
    fetchUpdate(): void {
        return;
    }

    fetchFind(): void {
        this.clientStore_Client = {};
    }

    fetchCreate(): void {
        this.clientStore_Client = {};
    }

    setClient( client: Client ): void {
        this.clientStore_Client = client;
    }

}

export default Alt.createStore( ClientStore, 'ClientStore' );