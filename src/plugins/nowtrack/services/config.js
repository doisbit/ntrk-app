// @flow

import { ConfigClass } from '../../../services/config';

class NowTrackConfig extends ConfigClass {
    
    constructor(){
        super();

        this.mergeConfig({
            API: {
                LOCATION: '/location',
                DRIVERS: '/drivers',
                DRIVER: '/drivers/:id',
                CLIENTS: '/clients',
                CLIENT: '/clients/:id',
                DELIVERIES: '/deliveries',
                DELIVERY: '/deliveries/:id'
            }
        });

    }

}

export default new NowTrackConfig;