import _ from 'lodash';

import Portuguese from './i18n/portuguese';
import English from './i18n/english';

import { i18nClass } from '../../../services/i18n';

class NowTracki18n extends i18nClass {
    
    constructor(){
        super();

        this.mergeDictionary( 'english', English );
        this.mergeDictionary( 'portuguese', Portuguese );

    }

}

export default new NowTracki18n;