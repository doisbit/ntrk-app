export default {
    deliveries: 'entregas',
    settings: 'configurações',
    skip: 'pular',
    you_are_a: 'você é um...',
    driver: 'motorista',
    commerce: 'comércio',
    delivery_statuses: {
        created: 'criada',
        assigned: 'assinada',
        on_the_way: 'à caminho',
        arrived: 'no local',
        delivered: 'entregue'
    },
    messages: {
        background_location_notification: "estamos usando sua localização"
    },
    client_form: {
        name: "Nome fantasia",
        document: "CNPJ",
        phone: "Telefone",
        zipcode: "CEP",
        address: "Endereço",
        city: "Cidade",
        state: "Estado" 
    },
    create: 'Criar',
    update_to: 'atualizar para',
    reject: 'rejeitar',
    open_on_maps: 'abrir no mapa',
    send_whats: 'enviar whatsapp'
};