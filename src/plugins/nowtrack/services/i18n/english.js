export default {
    deliveries: 'deliveries',
    settings: 'settings',
    skip: 'skip',
    you_are_a: 'you are a...',
    driver: 'driver',
    commerce: 'commerce',
    delivery_statuses: {
        my_deliveries: 'your deliveries',
        waiting_approval: 'waiting approval',
        created: 'created',
        assigned: 'assigned',
        on_the_way: 'on the way',
        arrived: 'arrived',
        delivered: 'delivered'
    },
    messages: {
        background_location_notification: "we're using your location"
    },
    client_form: {
        name: "Name",
        document: "Document",
        phone: "Phone",
        zipcode: "ZipCode",
        address: "Address",
        city: "City",
        state: "State" 
    },
    create: 'create',
    update_to: 'update to',
    reject: 'reject',
    open_on_maps: 'open on maps',
    send_whats: 'send whatsapp',
};