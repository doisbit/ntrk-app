import _ from 'lodash';
import { PixelRatio, Platform, Dimensions } from 'react-native';

import { StyleClass } from '../../../services/styles';

class NowTrackStyle extends StyleClass {

    constructor() {
        super();

        this.mergeColors({
            sushi: '#8cc63f'
        });

        this.mergeStyles({
            landing: {
                pane: {
                    ...this.get('full_width flex'),
                    paddingTop: 70,
                    paddingBottom: 55,
                    paddingLeft: 15,
                    paddingRight: 15,
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    backgroundColor: this.colors.sushi
                }
            }
        })

    }

}

export default new NowTrackStyle;