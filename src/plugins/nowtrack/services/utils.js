/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Config from '../../../services/config';

import { UtilsClass } from '../../../services/utils';

class NowTrackUtils extends UtilsClass {

    constructor(){
        super();
    }

    orderDeliveries(driver, deliveries){
        return deliveries.sort((a, b) => {
            return (
                this.distanceBetweenCoordinates(driver.lat, driver.lng, a.customer.lat, a.customer.lng) -
                this.distanceBetweenCoordinates(driver.lat, driver.lng, b.customer.lat, b.customer.lng)
            )
        })
    }

    /**
     * 
     * This routine calculates the distance between two points
     * in Kilometers (given the latitude/longitude of those points).
     * 
     * @param {String} lat1 first latitude to calculate
     * @param {String} lon1  first longitude to calculate
     * @param {String} lat2  second latitude to calculate
     * @param {String} lon2  first longitude to calculate
     * @return {String} The distance
     */
    distanceBetweenCoordinates( lat1, lon1, lat2, lon2 ){
        let radlat1 = Math.PI * lat1 / 180;
        let radlat2 = Math.PI * lat2 / 180;
        let theta = lon1 - lon2;
        let radtheta = Math.PI * theta / 180;
        let dist = Math.sin( radlat1 ) * Math.sin( radlat2 ) + Math.cos( radlat1 ) * Math.cos( radlat2 ) * Math.cos( radtheta );
        dist = Math.acos( dist );
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return dist;
    }
    
    decodePoints( t, e ){
        for(var n,o,u=0,l=0,r=0,d= [],h=0,i=0,a=null,c=Math.pow(10,e||5);u<t.length;){a=null,h=0,i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);n=1&i?~(i>>1):i>>1,h=i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);o=1&i?~(i>>1):i>>1,l+=n,r+=o,d.push([l/c,r/c])}return d=d.map(function(t){return{latitude:t[0],longitude:t[1]}})
    }


}

export default new NowTrackUtils;