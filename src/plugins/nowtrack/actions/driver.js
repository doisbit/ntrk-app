/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import * as React from "react";

import Alt from '../../../vendors/alt';
import Config from '../services/config';
import Api from '../../../services/api';
import CommonsActions from '../../../actions/commons';
import AuthActions from '../../../actions/auth';

type Driver = {
    id?: string, // "d999e1"
    status?: number, // 1
    phone?: string, // "48991674935"
    device_type?: string, // "iPhone"
    device_version?: string, // "11.01"
    userId?: string, // "80e28cd4-78d0-46fa-91f7-9635dfba58fa"
    updatedAt?: string, // "2017-12-06T11:43:02.940Z"
    createdAt?: string // "2017-12-06T11:43:02.940Z"
};

/**
 * DriverActions
 * 
 * Class containing actions related to the driver
 * 
 * @author Alexandre Moraes
 */
class DriverActions {

    /**
     * Creates a driver
     * 
     * @param {Object} api The API object already authenticated
     */
    create( api: any, body?: Object = {} ): any {
        return async ( dispatch ) => {
            // dispatch();
            try{
                let response = await api.post( Config.get( 'API.DRIVERS' ), body );
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setDriver( response.data );
            }
            catch ( ERR ){
                return CommonsActions.throwError( ERR );
            }
        }
    }

    /**
     * Returns the first driver that matches the query search
     * 
     * @param {Object} api The API object already authenticated
     * @param {Object} query The key/value dict to find
     */
    find( api: any, query?: Object = {} ): any {
        return async ( dispatch ) => {
            dispatch();
            try{
                let response = await api.get( Config.get( 'API.DRIVERS' ), query );
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setDriver( response.data[ 0 ] );
            }
            catch( ERR ){

            }
        }
    }

    
    /**
     * Updates a driver
     * 
     * @param {Object} api The API object already authenticated
     */
    update( api: any, driverId: string, body?: Object = {} ) {
        return async ( dispatch ) => {
            dispatch();
            try{
                let response = await api.put( Config.get( 'API.DRIVER' ).replace( ':id', driverId ), body )
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setDriver( response.data );
            }
            catch( ERR ){
                return CommonsActions.throwError( ERR );
            }
        }
    }

    /**
     * Set the coords from the Driver
     * 
     * @param {Array} coords An array containing LAT and LNG values
     */
    setLocation(coords){
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( [coords.latitude, coords.longitude] );
                })
            }
            else {
                dispatch( [coords.latitude, coords.longitude] );
            }
        }
    }

    setDriver( driver: Driver ): Driver {
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( driver );
                })
            }
            else {
                dispatch( driver );
            }
        }
    }

}

export default Alt.createActions(DriverActions);