/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import * as React from "react";

import Alt from '../../../vendors/alt';
import Config from '../services/config';
import Api from '../../../services/api';
import CommonsActions from '../../../actions/commons';
import AuthActions from '../../../actions/auth';

type Client = {
    id?: string, //"005c3cf4-f231-4c81-b071-583737dfa05c",
    name?: string, //"Kikoni",
    document?: string, //"26896836000101",
    address?: string, //"Rua Rafael Bandeira, 328",
    phone?: string, //"40429662",
    zipcode?: string, //"88056125",
    city?: string, //"Florianópolis",
    state?: string, //"Santa Catarina",
    userId?: string, //"256652ea-07a3-44ef-90a5-3bf5c548431f",
    status?: number, //1,
    updatedAt?: string, //"2017-12-05T21:31:45.681Z",
    createdAt?: string, //"2017-12-05T21:31:45.681Z"
};

/**
 * ClientActions
 * 
 * Class containing actions related to the client
 * 
 * @author Alexandre Moraes
 */
class ClientActions {

    /**
     * Creates a client
     * 
     * @param {Object} api The API object already authenticated
     */
    create( api: any, body?: Object = {} ): any {
        return async ( dispatch ) => {
            try{
                let response = await api.post( Config.get( 'API.CLIENTS' ), body );
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setClient( response.data );
            }
            catch ( ERR ){
                return CommonsActions.throwError( ERR );
            }
        }
    }

    /**
     * Returns the first client that matches the query search
     * 
     * @param {Object} api The API object already authenticated
     * @param {Object} query The key/value dict to find
     */
    find( api: any, query?: Object = {} ): any {
        return async ( dispatch ) => {
            try{
                let response = await api.get( Config.get( 'API.CLIENTS' ), query );
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setClient( response.data[ 0 ] );
            }
            catch( ERR ){
                return CommonsActions.throwError( ERR );
            }
        }
    }

    /**
     * Updates a client
     * 
     * @param {Object} api The API object already authenticated
     */
    update( api: any, clientId: string, body?: Object = {} ) {
        return async ( dispatch ) => {
            try{
                let response = await api.put( Config.get( 'API.CLIENT' ).replace( ':id', clientId ), body )
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setClient( response.data );
            }
            catch( ERR ){
                return CommonsActions.throwError( ERR );
            }
        }
    }

    setClient( client: Client ): Client {
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( client );
                })
            }
            else {
                dispatch( client );
            }
        }
    }

}

export default Alt.createActions(ClientActions);