/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import * as React from "react";

import Alt from '../../../vendors/alt';
import Config from '../services/config';
import CommonsActions from '../../../actions/commons';
import AuthActions from '../../../actions/auth';

import Utils from '../services/utils';

type Delivery = {
    deliveryId?: string, //"0888c7",
    clientId?: string, //"5c125277-dc82-4af4-b8dc-5e752dd8beeb",
    status?: number, //1,
    customerId?: string, //"cbdad9",
    id?: string, //"094952",
    updatedAt?: string, //"2017-12-06T11:33:40.256Z",
    createdAt?: string, //"2017-12-06T11:33:40.256Z"
};

/**
 * DeliveryActions
 * 
 * Class containing actions related to the delivery
 * 
 * @author Alexandre Moraes
 */
class DeliveryActions {

    /**
     * Creates a delivery
     * 
     * @param {Object} api The API object already authenticated
     */
    createDelivery( api: any, body?: Object = {} ): any {
        return async ( dispatch ) => {
            try{
                let response = await api.post( Config.get( 'API.DELIVERIES' ), body );
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setDelivery( response.data );
            }
            catch ( ERR ) {
                return CommonsActions.throwError( ERR );
            }
        }
    }

    /**
     * Get deliveries
     * 
     * @param {Object} api The API object already authenticated
     */
    getDeliveries( api: any, query?: Object = {}, driver?: Object = false ): any {
        return async ( dispatch ) => {
            try{
                let response = await api.get( Config.get( 'API.DELIVERIES' ), query );
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setDeliveries( driver ? Utils.orderDeliveries(driver, response.data) : response.data );
            }
            catch ( ERR ) {
                return CommonsActions.throwError( ERR );
            }
        }
    }

    /**
     * Return a polyline data to plot
     * into a MapView object containing the route
     * through the closest routes
     * 
     * @param {Array} from The coordinates to route from
     * @param {Array<Array>} to The array of coordinates to route to
     */
    getDeliveriesWaypoint(driver, deliveries){
        return async ( dispatch ) => {
            let mode = 'driving';
            let origin = [driver.lat, driver.lng].join(',');
            let waypoints = deliveries.map((d) => { return [d.customer.lat, d.customer.lng] });
            let destination = waypoints.pop().join(',');
            let APIKEY = Config.get('GOOGLE.API.MAPS');
            let url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&waypoints=${waypoints.join("|")}&destination=${destination}&key=${APIKEY}&mode=${mode}`;
            try {
                fetch(url)
                .then((response) => response.json())
                .then((response) => {
                    if(response.routes.length)
                        return this.setDeliveriesWaypoint(
                            Utils.decodePoints(response.routes[0].overview_polyline.points)
                        )
                })
            }
            catch( ERR ){
                return CommonsActions.throwError( ERR );
            }
        }
    }

    setDeliveriesWaypoint( waypoint ){
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( waypoint );
                })
            }
            else {
                dispatch( waypoint );
            }
        }
    }

    setDeliveries( deliveries: Array<Delivery> ): Array<Delivery> {
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( deliveries );
                })
            }
            else {
                dispatch( deliveries );
            }
        }
    }

    setDelivery( delivery: Delivery ): Delivery {
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( delivery );
                })
            }
            else {
                dispatch( delivery );
            }
        }
    }

}

export default Alt.createActions(DeliveryActions);