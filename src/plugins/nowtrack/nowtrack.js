// @flow

import DriverScreen from './routers/Driver/screen.js';
import ClientScreen from './routers/Client/screen.js';
import LandingScreen from './routers/Landing/screen.js';

class NowTrackSetup {

    /**
     * Called on /routers/index.js in order
     * to load the plugin routes
     */
    setupRoutes(){

        return {
            Driver: { screen: DriverScreen },
            Client: { screen: ClientScreen },
            Landing: { screen: LandingScreen },
        }

    }

}

export default new NowTrackSetup();