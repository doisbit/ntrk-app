// @flow

import * as React from "react";
import _ from 'lodash';

import ScreenComponent from '../../../components/screen';

import i18n from '../services/i18n';

import DeliveryActions from '../actions/delivery';
import DeliveryStore from '../stores/delivery';

import DriverActions from '../actions/driver';
import DriverStore from '../stores/driver';

import ClientActions from '../actions/client';
import ClientStore from '../stores/client';

import { Alert } from 'react-native';

//import BackgroundGeolocation from "react-native-background-geolocation";
import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';

import Config from '../services/config';

import Utils from '../services/utils';

class NowTrackScreenComponent extends ScreenComponent {

    _onChange: any;
    _skipLandingIfNotUserAnymore: any;
    _refreshUserStateIfChosenFormat: any;
    _startTracking: any;
    _ensureToken: any;

    DeliveryStatuses: Array<Object> = [
        {
            id: 1,
            label: _.capitalize(i18n.__('delivery_statuses.created'))
        },
        {
            id: 2,
            label: _.capitalize(i18n.__('delivery_statuses.assigned'))
        },
        {
            id: 3,
            label: _.capitalize(i18n.__('delivery_statuses.on_the_way'))
        },
        {
            id: 4,
            label: _.capitalize(i18n.__('delivery_statuses.arrived'))
        },
        {
            id: 5,
            label: _.capitalize(i18n.__('delivery_statuses.delivered'))
        }
    ];

    constructor(props: Object) {
        super(props);

        this.state = {
            ...this.getInitialState(),
            locationEnabled: false,
            locationIsMoving: false,
        }

        this.trackingInterval = null;

        this._ensureToken = this._ensureToken.bind(this);
        this._startTracking = this._startTracking.bind(this);

        this._apiTimeout = null;

        this._onChange = this._onChange.bind(this);
        this._skipLandingIfNotUserAnymore = this._skipLandingIfNotUserAnymore.bind(this);
        this._refreshUserStateIfChosenFormat = this._refreshUserStateIfChosenFormat.bind(this);
    }

    async _ensureToken() {
        try {
            if (_.has(this.state, 'authStore_User.driver')) {
                let deviceInfo = Utils.getDeviceInfo();
                let notificationToken = await Utils.getNotificationToken();
                await super.getApi().post(Config.get('API.PUSH_TOKENS'), {
                    type: deviceInfo.device_type,
                    token: notificationToken
                });
            }
        }
        catch (ERR) {
            return super.getCommonsActions().throwError(ERR);
        }
    }

    _getDeliveryLabelByStatus(status: number): string {
        return _.find(this.DeliveryStatuses, { id: status }).label;
    }

    _onChange(state: any) {
        if (!this._refreshUserStateIfChosenFormat(state) &&
            !this._skipLandingIfNotUserAnymore(state)) {
            super._onChange(state);
        }
    }

    /**
     * Checks if:
     * 
     * 1. State has 'driver' or 'client'
     * 2. User stills a normal user
     * 
     * Refresh user token on database in order to update his
     * status of not be a "user" anymore, but a driver/client.
     * 
     * _skipLandingIfNotUserAnymore shall trigger afterwards and
     * forward user to proper screen
     * 
     * @param {Object} state The state
     */
    _refreshUserStateIfChosenFormat(state: any) {
        if (Utils.isAuthenticated(state) && this.state.authStore_User.roleKey == 'user') {
            super.getAuthActions().refreshToken(this.state.authStore_Auth);
        }
        else {
            return false;
        }
    }

    /**
     * Checks if:
     * 
     * 1. User is authenticated
     * 2. User is a driver or a client
     * 3. User is at 'Landing'
     * 
     * If so, redirect user into proper screen
     * 
     * @param {Object} state The state
     */
    _skipLandingIfNotUserAnymore(state: any): any {
        if (this.props.navigation.state.key == 'Landing') {

            if (
                _.has(state, 'authStore_User.roleKey') &&
                ['driver', 'client'].indexOf(state.authStore_User.roleKey) > -1
            ) {
                if (
                    state.authStore_User.roleKey == 'driver' &&
                    !_.has(state, 'driverStore_Driver.id')
                ) {
                    DriverActions.find(super.getApi(), { userId: state.authStore_User.id });
                }

                if (
                    state.authStore_User.roleKey == 'client' &&
                    !_.has(state, 'clientStore_Client.id')
                ) {
                    ClientActions.find(super.getApi(), { userId: state.authStore_User.id });
                }

                return setImmediate(
                    this.props.navigation.navigate.bind(
                        this, _.capitalize(state.authStore_User.roleKey)
                    )
                )
            }
        }
    }

    _startTracking() {
        try {

            BackgroundGeolocation.on('location', DriverActions.setLocation);

            BackgroundGeolocation.on('error', (error) => {
                console.warn('[ERROR] BackgroundGeolocation error:', error);
            });

            BackgroundGeolocation.configure({
                desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
                stationaryRadius: 20,
                distanceFilter: 50,
                notificationTitle: 'NowTrack',
                notificationText: _.capitalize(i18n.__('messages.background_location_notification')),
                debug: false,
                startOnBoot: false,
                stopOnTerminate: true,
                locationProvider: BackgroundGeolocation.DISTANCE_FILTER_PROVIDER,
                interval: 1000,
                fastestInterval: 5000,
                activitiesInterval: 5000,
                maxLocations: 100,
                stopOnStillActivity: false,
                url: Config.get('API.ENDPOINT') + Config.get('API.LOCATION'),
                syncUrl: Config.get('API.ENDPOINT') + Config.get('API.LOCATION'),
                httpHeaders: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${this.state.authStore_Auth}`
                }
            });

            BackgroundGeolocation.on('authorization', (status) => {
                if (status !== BackgroundGeolocation.AUTHORIZED) {
                    Alert.alert(i18n.__('location.enable_question_title'), i18n.__('location.enable_question_description'), [
                        { text: 'Yes', onPress: () => BackgroundGeolocation.showLocationSettings() },
                        { text: 'No', onPress: () => Alert.alert(i18n.__('location.enable_denied')), style: 'cancel' }
                    ]);
                }
                else {
                    BackgroundGeolocation.start();
                }
            });

            BackgroundGeolocation.checkStatus(status => {
                if (!status.isRunning) {
                    BackgroundGeolocation.start();
                }
            });

        }
        catch (ERR) {
            return super.getCommonsActions().throwError(ERR);
        }
    }

    getInitialState() {
        return {
            ...super.getInitialState(),
            ...DriverStore.getState(),
            ...DeliveryStore.getState(),
            ...ClientStore.getState(),
        }
    }

    componentDidMount() {
        super.componentDidMount();
        DriverStore.listen(this._onChange);
        DeliveryStore.listen(this._onChange);
        ClientStore.listen(this._onChange);
        this._skipLandingIfNotUserAnymore(this.state);
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        DriverStore.unlisten(this._onChange);
        DeliveryStore.unlisten(this._onChange);
        ClientStore.unlisten(this._onChange);
        // Unlisten to BackgroundGeolocation events
        BackgroundGeolocation.events.forEach(event => BackgroundGeolocation.removeAllListeners(event));
        // Fully destroy BackgroundGeolocation
        BackgroundGeolocation.stop();
    }

    getDriverStore(): any {
        return DriverStore;
    }

    getDriverActions(): any {
        return DriverActions;
    }

    getDeliveryStore(): any {
        return DeliveryStore;
    }

    getDeliveryActions(): any {
        return DeliveryActions;
    }

    getClientStore(): any {
        return ClientStore;
    }

    getClientActions(): any {
        return ClientActions;
    }

}

export default NowTrackScreenComponent;