/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import _ from 'lodash';
import { Platform, Linking, PermissionsAndroid } from 'react-native';
import { PixelRatio, Dimensions } from 'react-native';
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';

import Config from './config';

type LocationData = {
    coords?: {
        latitude: number,
        longitude: number,
        altitude: number,
        accuracy: number,
        heading: number,
        speed: number,
    },
    timestamp?: number,
};

class Utils {

    formats: Object;

    constructor() {

        /*
        // This shall be called regardless of app state: running, background or not running. Won't be called when app is killed by user in iOS
        FCM.on(FCMEvent.Notification, async (notif) => {
            // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
            if(notif.opened_from_tray){
                //iOS: app is open/resumed because user clicked banner
                //Android: app is open/resumed because user clicked banner or tapped app icon
            }
        });

        FCM.on(FCMEvent.RefreshToken, (token) => {
            console.log(token);
            // fcm token may not be available on first load, catch it here
        });
        */

        this.formats = {
            email: [
                {
                    rgx: (string) => { return !_.isNil(string) && string.length > 0 },
                    msg: 'VALIDATE.EMPTY_EMAIL'
                },
                /*{
                    rgx: ( string ) => {  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3 }\.[0-9]{ 1,3 }\.[0-9]{ 1,3 }\.[0-9]{ 1,3 }])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{ 2, }))$/.test( string ) },
                    msg: 'VALIDATE.INVALID_EMAIL'
                }*/
            ],
            password: [
                {
                    rgx: (string) => { return !_.isNil(string) && string.length > 0 },
                    msg: 'VALIDATE.EMPTY_PASSWORD'
                },
                /*{
                    rgx: (  string ) => {  return /^((?=.*\d )(?=.*[a-z])(?=.*[A-Z]).{6,15 })$/.test( string ) },
                    msg: 'VALIDATE.INVALID_PASSWORD'
                }*/
            ]
        }

    }

    getScreenDimensions(): Object {
        return Dimensions.get('window');
    }

    validate(format: string, string: string): Promise<void> {
        return new Promise((resolve, reject) => {
            let done = _.after(this.formats[format].length, () => {
                return resolve();
            });

            _.each(this.formats[format], (c) => {
                if (!c.rgx(string)) reject(new Error(c.msg));
                done();
            });
        })
    }

    isAuthenticated(state) {
        return _.has(state, 'driverStore_Driver.id') || _.has(state, 'clientStore_Client.id');
    }

    getNotificationToken(): Promise<String> {
        return new Promise(async (resolve, reject) => {
            try {
                await FCM.requestPermissions();
                let token = await FCM.getFCMToken();
                resolve(token);
            }
            catch (ERR) {
                reject(ERR);
            }
        });
    }

    canOpen(scheme: string): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            let canOpenIt;
            let mapScheme = Platform.OS === 'ios' ? 'maps:' : 'geo:';
            try {
                switch (scheme) {
                    case ('whatsapp'):
                        canOpenIt = await Linking.canOpenURL(`whatsapp://send?text=bla&phone=551234567890`);
                        if (canOpenIt) return resolve(true);
                    case ('maps'):
                        canOpenIt = await Linking.canOpenURL(`${mapScheme}-27.57959,-48.525497`);
                        if (canOpenIt) return resolve(true);
                    default:
                        reject();
                        break;
                }
            }
            catch (ERR) {
                reject(ERR);
            }
        });
    }

    sendWhats(phone: number, message: string): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                Linking.openURL(`whatsapp://send?text=${message}&phone=${phone}`);
                resolve();
            }
            catch (ERR) {
                reject(ERR);
            }
        })
    }

    /**
     * Programatically opens a coordinate on the default map app
     * from device
     * 
     * @param {number} lat The latitude coordinate
     * @param {number} lng The longitude coordinate
     */
    openMap(lat: number, lng: number): Promise<any> {
        return new Promise(async (resolve, reject) => {
            let scheme = Platform.OS === 'ios' ?
                'http://maps.apple.com/maps?daddr=' :
                'http://maps.google.com/maps?daddr=';
            let url = `${scheme}${lat},${lng}`;
            try {
                Linking.openURL(url);
                resolve();
            }
            catch (ERR) {
                reject(ERR);
            }
        });
    }

    /**
     * Asks for the user location and return the current location
     */
    getUserLocation(): Promise<LocationData> {
        return new Promise(async (resolve, reject) => {
            try {

                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
                );

                if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
                    throw new Error('Permission denied');
                }

                navigator.geolocation.getCurrentPosition((pos) => {
                    return resolve(pos.coords);
                }, (err) => {
                    throw new Error(err.message);
                });
            }
            catch (ERR) {
                reject(ERR);
            }
        });
    }

    getDeviceInfo(): Object {
        let output: Object = {};
        return {
            device_type: Platform.OS,
            device_version: "@TODO"
        };
    }

}

export default new Utils;

exports.UtilsClass = Utils;