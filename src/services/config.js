/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import _ from 'lodash';

class Config {

    config: Object;

    constructor(){
        this.config = {
            FIRST_ROUTE: 'Landing',
            PLUGINS: [
                'nowtrack'
            ],
            DB: {
                NAME: 'myapp.db',
            },
            API: {
                INFO: '/info',
                ENDPOINT: 'http://172.16.48.212:3005',
                AUTH: {
                    SIGNUP: '/auth/signup',
                    TOKEN: '/auth/token',
                    REFRESH_TOKEN: '/auth/refresh',
                },
                OAUTH: {
                    LOGIN: '/oauth/login',
                },
                USER: '/user/:id',
                USERS: '/users',
                PUSH_TOKENS: '/push_tokens'
            },
            AUTH: {
                ENABLE_LOCAL_AUTH: true,
                ENABLE_FACEBOOK_SIGNIN: false,
            },
            GOOGLE: {
                API: {
                    MAPS: 'AIzaSyB0l9U1nbHI3w1wnDYKrEBZpwq3WcM7A-4'
                }
            },
            FB: {
                CLIENT_ID: '1201211719949057',
                AUTH_PERMISSIONS: [ 'public_profile', 'email', 'user_birthday' ]
            },
            SENTRY: {
                IOS: 'https://14c536560fcb4ae7a6849500575b675d:03cb4b05e1d04531bc0a75678f820a7b@sentry.io/253441',
                ANDROID: 'https://14c536560fcb4ae7a6849500575b675d:03cb4b05e1d04531bc0a75678f820a7b@sentry.io/253441',
                PUBLIC_DSN: 'https://14c536560fcb4ae7a6849500575b675d@sentry.io/253441'
            },
        }
    }

    get( key: string ): string {
        return _.get( this.config, key );
    }
    
    mergeConfig( config: Object ): void {
        this.config = _.merge( this.config, config );
    }

}

export default new Config;

exports.ConfigClass = Config;