/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

export default {
    facebook_login: "Facebook Login",
    errors: {
        N_F: "Nothing like that was found here, sorry",
        SYS_ERR: "There was an error in our servers, try again later",
        SIGNUP_MAIL_TKN: "This e-mail is already in use",
        OAUTH_REVOKE: "Authentication revoked by user",
        AUTH_ERROR: "An error occurred during the authentication",
        NOT_CONNECTED: "You're not connected to the internet.",
        WRNG_TKN: "Your credentials was declined.",
        VALIDATE: {
            EMPTY_EMAIL: "You must type an email",
            EMPTY_PASSWORD: "You must type a password",
            INVALID_EMAIL: "Invalid email address",
            INVALID_PASSWORD: "Your password don't attend the minimum security requirements. Be more creative please."
        }
    },
    messages: {
        maintenance: "We're currently passing through a maintenance in our servers.\n\nPlease try again later."
    },
    label: {
        email: 'E-mail',
        password: 'Password'
    },
    login: 'Login',
    signup: 'Create account',
    login_instead: 'Already registered',
};