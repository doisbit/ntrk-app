/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

export default {
    facebook_login: "Login via Facebook",
    errors: {
        N_F: "Oops! Nada encontrado para sua requisição.",
        SYS_ERR: "Houve um erro em nossos servidores. Tente novamente mais tarde.",
        SIGNUP_MAIL_TKN: "Este e-mail já está em uso",
        OAUTH_REVOKE: "Autenticação cancelada pelo usuário",
        AUTH_ERROR: "Houve um erro durante a autenticação",
        NOT_CONNECTED: "Você não está conectado",
        WRNG_TKN: "Credenciais rejeitadas.",
        VALIDATE: {
            EMPTY_EMAIL: "O E-mail não pode ficar em branco",
            EMPTY_PASSWORD: "A senha não pode ficar em branco",
            INVALID_EMAIL: "E-mail inválido",
            INVALID_PASSWORD: "Sua senha não atende os requisitos mínimos de segurança"
        }
    },
    messages: {
        maintenance: "Estamos atualmente passando por manutenção em nossos servidores.\n\nTente novamente mais tarde."
    },
    label: {
        email: 'E-mail',
        password: 'Senha'
    },
    login: 'Entrar',
    signup: 'Criar conta',
    login_instead: 'Já tenho conta'
}