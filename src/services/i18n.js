/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import _ from 'lodash';
import DeviceInfo from 'react-native-device-info';

import Portuguese from './i18n/portuguese';
import English from './i18n/english';

class i18n {
    
    deviceLocale: string;
    dictionary: Object;

    constructor(){
        this.getDeviceLocale();

        this.dictionary = {
            'english': English,
            'portuguese': Portuguese
        }

    }

    async getDeviceLocale(): void {
        this.deviceLocale = DeviceInfo.getDeviceLocale();
    }

    localeProxy( deviceLocale: string ): string {
        let proxy = {
            'en-BR': 'english',
            'pt-BR': 'portuguese'
        };
        if( !_.has( proxy, deviceLocale ) ) return 'english';
        return proxy[ deviceLocale ];
    }
    
    mergeDictionary( language: string, dictionary: Object ): void {
        this.dictionary[ language ] = _.merge( this.dictionary[ language ], dictionary );
    }

    __( string: string ): string {
        if( !_.has( this.dictionary[ this.localeProxy( this.deviceLocale ) ], string ) ) return string;        
        return _.get( this.dictionary[ this.localeProxy( this.deviceLocale ) ], string );
    }

}

export default new i18n;

exports.i18nClass = i18n;