import RNFirebase from 'react-native-firebase';
import Config from './config';

export default RNFirebase.initializeApp( Config.get( 'firebase' ) );