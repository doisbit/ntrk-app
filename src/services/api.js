/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Config from './config';
import _ from 'lodash';

type ApiResponse = {
    status: boolean,
    data: Object,
    user: boolean,
    message: string
}
class Api {

    endpoint: string;
    headers: Object;

    constructor(){

        this.endpoint = Config.get("API.ENDPOINT");
        
        this.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }

    }

    queryFromJson( json: Object = {} ): string {
        return Object.keys( json ).map( ( k ) => {
            return `?${encodeURIComponent( k )}=${encodeURIComponent( json[ k ] )}`
        }).join( '&' )
    }
    
    /**
     * Sets the Authorization header on api requests
     * 
     * @param {string} token The user JWT token
     */
    setJWT( token: string ): void {
        this.headers.Authorization = `Bearer ${ token }`;
    }

    dropJWT(): void {
        delete this.headers.Authorization;
    }

    /**
     * Gets the already set Authorization token
     */
    getJWT(): string {
        if( !_.has( this.headers, 'Authorization' ) ){
            return "";
        }
        else{
            return this.headers.Authorization.split(' ')[1];
        }
    }
    
    /**
     * Execute a GET request
     * 
     * @param {string} url The URL to get
     * @param {Object} query The URL query if exists
     * @return {Promise<ApiResponse>} The response object
     */
    get( url: string, query?: Object = {}): Promise<ApiResponse> {
        return new Promise( ( resolve: any, reject: any ) => {
            console.log( `GET request to ${this.endpoint+url}` );
            console.log( `Authenticated? ${ _.has( this.headers, 'Authorization' ) }`);
            console.log( `Query: ${JSON.stringify( query ) }`);
            fetch( `${this.endpoint}${url}${this.queryFromJson( query )}`, {
                method: 'GET',
                headers: this.headers
            } )
            .then( ( response: any ) => response.json() )
            .catch( ( err: any ) => {
                reject( err );
            } )
            .then( ( response: ApiResponse<Object> ) => {
                console.log( response );
                resolve( response );
            } );
        } )
    }

    /**
     * Execute a POST request
     * 
     * @param {string} url The URL to post
     * @param {Object} body The POST body
     * @param {Object} query The URL query if exists
     * @return {Promise<ApiResponse>} The response object
     */
    post( url: string, body?: Object, query?: Object ): Promise<ApiResponse> {
        return new Promise( ( resolve: any, reject: any ) => {
            console.log( `POST request to ${this.endpoint+url}` );
            console.log( `Authenticated? ${ _.has( this.headers, 'Authorization' ) ? this.headers.Authorization : false}`);
            console.log( `Body: ${JSON.stringify( body || {} ) }`);
            fetch( `${this.endpoint}${url}${this.queryFromJson( query )}`, {
                method: 'POST',
                headers: this.headers,
                query: JSON.stringify( query || { } ),
                body: JSON.stringify( body || { } )
            } )
            .then( ( response: any ) => response.json() )
            .catch( ( err: any ) => {
                console.log( err );
                reject(  err );
            } )
            .then( ( response: ApiResponse<Object> ) => {
                console.log( response );
                resolve( response );
            } );
        } )
    }
    
    /**
     * Execute a DELETE request
     * 
     * @param {string} url The URL to get
     * @param {Object} query The URL query if exists
     * @return {Promise<ApiResponse>} The response object
     */
    delete( url: string, query?: Object ): Promise<ApiResponse> {
        return new Promise( ( resolve: any, reject: any ) => {
            console.log( `DELETE request to ${this.endpoint+url}` );
            console.log( `Authenticated? ${ _.has( this.headers, 'Authorization' ) }`);
            fetch( `${this.endpoint}${url}${this.queryFromJson( query )}`, {
                method: 'DELETE',
                headers: this.headers,
                query: JSON.stringify( query || { } ),
            } )
            .then( ( response: any ) => response.json() )
            .catch( ( err: any ) => {
                reject( err );
            } )
            .then( ( response: ApiResponse<Object> ) => {
                console.log( response );
                resolve( response );
            });
        })
    }
    
    /**
     * Execute a PUT request
     * 
     * @param {string} url The URL to get
     * @param {Object} body The PUT body
     * @param {Object} query The URL query if exists
     * @return {Promise<ApiResponse>} The response object
     */
    put( url: string, body?: Object, query?: Object ): Promise<ApiResponse> {
        return new Promise( ( resolve: any, reject: any ) => {
            console.log( `PUT request to ${this.endpoint+url}` );
            console.log( `Authenticated? ${ _.has( this.headers, 'Authorization' ) }`);
            console.log( `Body: ${JSON.stringify( body || {} ) }`);
            fetch( `${this.endpoint}${url}${this.queryFromJson( query )}`, {
                method: 'PUT',
                headers: this.headers,
                body: JSON.stringify( body || { } ),
                query: JSON.stringify( query || { } ),
            } )
            .then( ( response: any ) => response.json() )
            .catch( ( err: any ) => {
                console.log( err );
                reject( err );
            })
            .then( ( response ) => {
                console.log( response );
                resolve( response );
            });
        })
    }

}

export default new Api;