/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import _ from 'lodash';
import { PixelRatio, Platform, Dimensions } from 'react-native';

class Style {

    guidelineBaseWidth: number;
    guidelineBaseHeight: number;
    screenWidth: number;
    screenHeight: number;
    colors: Object;
    styles: Object;

    constructor(){

        this.guidelineBaseWidth = 350;
        this.guidelineBaseHeight = 680;
        this.screenWidth = Dimensions.get('window').width;
        this.screenHeight = Dimensions.get('window').height;

        this.colors = {
            splash_primary: '#17C3F5',
            primary: '#607D8B',
            dark_primary: '#455A64',
            light_primary: '#CFD8DC',
            accent: '#FF5252',
            white: '#FFFFFF',
            black: '#000000',
            primary_text: '#212121',
            secondary_text: '#757575',
            divider: '#BDBDBD',
            facebook_blue: '#3b5998',
            footer_icon: '#555555',
            android: {
                footer_icon: '#FFFFFF'
            }
        }

        this.colors.get = (color: string) => {
            if( _.has( this.colors[ Platform.OS ], color ) ){
                return this.colors[ Platform.OS ][color];
            }
            else{
                return this.colors[color];
            }
        }

        this.styles = {
            justify: {
                space_between: {
                    justifyContent: 'space-between'
                },
                center: {
                    justifyContent: 'center'
                },
                space_around: {
                    justifyContent: 'space-around'
                }
            },
            header: {
                backgroundColor: this.colors.dark_primary,
            },
            footer: {
                height: 67
            },
            statusbar: {
                barStyle: 'light-content'
            },
            flex: {
                flex: 1
            },
            flex_row: {
                flexDirection: 'row',
            },
            flex_col: {
                flexDirection: 'column',
            },
            font: {
                small: {
                    fontSize: this.scale( 12 )
                },
                large: {
                    fontSize: this.scale( 20 )
                },
                xl: {
                    fontSize: this.scale( 52 ),
                    fontWeight: 'bold'
                }
            },
            padding: {
                left: {
                    _5: { paddingLeft: this.scale( 5 ) },
                    _10: { paddingLeft: this.scale( 10 ) },
                    _15: { paddingLeft: this.scale( 15 ) },
                },
                top: {
                    _5: { paddingTop: this.scale( 5 ) },
                    _10: { paddingTop: this.scale( 10 ) },
                    _15: { paddingTop: this.scale( 15 ) },
                },
                right: {
                    _5: { paddingRight: this.scale( 5 ) },
                    _10: { paddingRight: this.scale( 10 ) },
                    _15: { paddingRight: this.scale( 15 ) },
                },
                bottom: {
                    _5: { paddingBottom: this.scale( 5 ) },
                    _10: { paddingBottom: this.scale( 10 ) },
                    _15: { paddingBottom: this.scale( 15 ) },
                },
                _30: {
                    padding: this.scale( 30 )
                },
                _5: {
                    padding: this.scale( 5 )
                },
                _10: {
                    padding: this.scale( 10 )
                }
            },
            border: {
                square: {
                    borderRadius: 0
                },
                none: {
                    borderWidth: 0,
                    borderColor: 'transparent'
                }
            },
            background: {
                splash_primary: {
                    backgroundColor: this.colors.splash_primary
                },
                primary: {
                    backgroundColor: this.colors.primary,
                },
                dark_primary: {
                    backgroundColor: this.colors.dark_primary,
                },
                light_primary: {
                    backgroundColor: this.colors.light_primary,
                },
                accent: {
                    backgroundColor: this.colors.accent,
                },
                white: {
                    backgroundColor: this.colors.white,
                }
            },
            color: {
                dark_primary: {
                    color: this.colors.dark_primary
                },
                accent: {
                    color: this.colors.accent
                },
                light_primary: {
                    color: this.colors.light_primary
                },
                primary_text: {
                    color: this.colors.primary_text
                },
                primary: {
                    color: this.colors.primary
                },
                secondary_text: {
                    color: this.colors.secondary_text
                },
                white: {
                    color: this.colors.white
                },
                divider: {
                    color: this.colors.divider
                }
            },
            self_vertical_center: {
                alignSelf: 'center'
            },
            self_vertical_bottom: {
                alignSelf: 'flex-end'
            },
            vertical_center: {
                justifyContent: 'center'
            },
            vertical_bottom: {
                justifyContent: 'flex-end'
            },
            text_center: {
                alignItems: 'center',
                textAlign: 'center'
            },
            text_right: {
                alignItems: 'flex-end'
            },
            push_center: {
                alignSelf: 'center'
            },
            push_right: {
                alignSelf: 'flex-end'
            },
            push_left: {
                alignSelf: 'flex-start'
            },
            full_width: {
                width: this.screenWidth,
            },
            img_large: {
                width: this.scale( 120 ),
                height: this.scale( 120 )
            },
            btn: {
                facebook: {
                    login: {
                        backgroundColor: this.colors.facebook_blue
                    }
                },
                square: {
                    lg: {
                        width: 100,
                        height: 100,
                        justifyContent: 'center'
                    }
                }
            },
            margin: {
                top: {
                    '_30': {
                        marginTop: this.scale( 30 )
                    },
                    '_10': {
                        marginTop: this.scale( 10 )
                    },
                    '_20': {
                        marginTop: this.scale( 20 )
                    },
                    '_5': {
                        marginTop: this.scale( 5 )
                    }
                },
                bottom: {
                    '_30': {
                        marginBottom: this.scale( 30 )
                    },
                    '_20': {
                        marginBottom: this.scale( 20 )
                    },
                    '_10': {
                        marginBottom: this.scale( 10 )
                    },
                    '_5': {
                        marginBottom: this.scale( 5 )
                    }
                },
                right: {
                    '_30': {
                        marginRight: this.scale( 30 )
                    },
                    '_5': {
                        marginRight: this.scale( 5 )
                    }
                },
                left: {
                    '_30': {
                        marginLeft: this.scale( 30 )
                    },
                    '_5': {
                        marginLeft: this.scale( 5 )
                    }
                },
                left: {
                    _0: {
                        marginLeft: 0
                    }
                }
            },
            header_area: {
                backgroundColor: this.colors.dark_primary,
                color: this.colors.white
            },
            highlight_area: {
                backgroundColor: this.colors.primary,
                color: this.colors.light_primary
            },
            content: {
                centered: {
                    padding: 10,
                    alignItems: 'center',
                    justifyContent:'center',
                    flex: 1
                }
            },
            grid: {
                centered: {
                    alignItems: 'center',
                    justifyContent:'center',
                }
            },
            ios: {
                
            },
            android: {
                font: {
                    small: {
                        fontSize: this.scale( 12 )
                    },
                    large: {
                        fontSize: this.scale( 17 )
                    },
                    xl: {
                        fontSize: this.scale( 37 ),
                        fontWeight: 'bold'
                    }
                },
            }
        };
    }

    luminance( hex: string, lum: number ): string {
        
        hex = String( hex ).replace(/[^0-9a-f]/gi, '');
        if ( hex.length < 6 ) {
            hex = hex[ 0 ]+hex[ 0 ]+hex[ 1 ]+hex[ 1 ]+hex[ 2 ]+hex[ 2 ];
        }
        lum = lum || 0;
    
        let rgb = "#", c, i;
        for ( i = 0; i < 3; i++ ) {
            c = parseInt( hex.substr( i*2, 2 ), 16 );
            c = Math.round( Math.min( Math.max( 0, c + ( c * lum ) ), 255 ) ).toString( 16 );
            rgb += ( "00"+c ).substr( c.length );
        }
    
        return rgb;
    }

    mergeColors( colors: Object ): void {
        this.colors = _.merge( this.colors, colors );
    }
    
    mergeStyles( styles: Object ): void {
        this.styles = _.merge( this.styles, styles );
    }

    scale( size: number ): number {
        return this.screenWidth / this.guidelineBaseWidth * size;
    }

    verticalScale( size: number ): number {
        return this.screenHeight / this.guidelineBaseHeight * size;
    }

    moderateScale( size: number, factor: number = 0.5 ): number {
        return size + ( this.scale( size ) - size ) * factor;
    }

    get( style: string ): Object {
        let output = { };
        style.split(' ').forEach( ( k ) => {
            if( _.has( this.styles[ Platform.OS ], k ) ){
                output = _.merge( output, _.get( this.styles[Platform.OS], k ) );
            }
            else{
                output = _.merge( output, _.get( this.styles, k ) );
            }
        });
        return output;
    }

}

export default new Style;

exports.StyleClass = Style;