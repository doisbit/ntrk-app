/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import * as React from "react";
import _ from 'lodash';
import { Image } from "react-native";
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Item, Input, Text, Button, Content } from "native-base";

import Container from '../components/container';

import ScreenComponent from '../components/screen';
import Style from '../services/styles';
import Config from '../services/config';
import Utils from '../services/utils';
import i18n from '../services/i18n';

export default class SignupScreen extends ScreenComponent {

    static navigationOptions = {
        drawerLockMode: 'locked-closed',
    };

    _handleSignup: any = this._handleSignup.bind( this );
    _onChange: any = this._handleSignup.bind( this );

    state: Object = {
        ...this.getInitialState()
    }

    getInitialState(): Object {
        return {
            ...super.getInitialState(),
            signup_email: null,
            signup_password: null
        }
    }

    componentDidMount(): void {
        super.componentDidMount();
        super.getCommonsActions().spinner(false);
    }

    componentWillUnmount(): void {
        super.componentWillUnmount()
    }

    async _handleSignup(): Promise<void> {
        try {

            await Utils.validate( 'email', this.state.signup_email );
            await Utils.validate( 'password', this.state.signup_password );

            super.getAuthActions().fetchSignup( {
                email: this.state.signup_email,
                password: this.state.signup_password
            } );
        }
        catch ( ERR ) {
            super.getCommonsActions().throwError( ERR );
        }
    }

    render(): any {
        return (
            <Container style={ Style.get(' background.splash_primary ') }>
                <Content contentContainerStyle={ Style.get(' content.centered ') }>
                    <Grid style={ Style.get(' grid.centered ') }>
                        <Col>
                            <Image
                                source={ require('../assets/img/splash-logo.png') }
                                resizeMode='contain'
                                style={ Style.get(' img_large push_center margin.bottom._30 ') }
                            />
                            <Item
                                regular
                                style={ Style.get(' margin.left._0 push_center background.white ') }>
                                <Input
                                    keyboardType='email-address'
                                    onChangeText={ ( v ) => this.setState({ signup_email: v  })}
                                    value={ this.state.signup_email }
                                    style={ Style.get(' color.dark_primary border.none ') }
                                    placeholder={ i18n.__('label.email') }
                                />
                            </Item>
                            <Item regular
                                style={ Style.get(' margin.left._0 push_center background.white ') }>
                                <Input
                                    secureTextEntry={ true }
                                    onChangeText={ ( v ) => this.setState({ signup_password: v  })}
                                    value={ this.state.signup_password }
                                    style={ Style.get(' color.dark_primary border.none ') }
                                    placeholder={ i18n.__('label.password') }
                                />
                            </Item>
                            <Button
                                onPress={ this._handleSignup }
                                block
                                style={ Style.get(' background.accent border.square ') }>
                                <Text style={ Style.get(' color.white ') }>
                                    { i18n.__('signup').toUpperCase() }
                                </Text>
                            </Button>
                            <Button
                                onPress={ () => { this.props.navigation.navigate('Auth')  }}
                                transparent
                                style={ Style.get(' margin.top._30 push_center border.square ') }>
                                <Text style={ Style.get(' font.small color.white ') }>
                                    { i18n.__('login_instead').toUpperCase() }
                                </Text>
                            </Button>
                        </Col>
                    </Grid>
                </Content>
            </Container>
        );
    }
}