/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */
import * as React from "react";
import _ from 'lodash';
import { View, Image, TouchableOpacity } from 'react-native';
import { Header, Left, Text, Button, Icon, Body, Title, Right, Content } from "native-base";

import Container from '../components/container';

import ScreenComponent from '../components/screen';
import Style from '../services/styles';
import i18n from '../services/i18n';

export default class MessagesScreen extends ScreenComponent {

    static navigationOptions = {
        drawerLockMode: 'locked-closed',
    };

    state: Object = {
        ...this.getInitialState(),
        unreadedMessages: false,
        commonsStore_isUnreadedReaded: false
    }

    _handleReadMessages: void = this._handleReadMessages.bind( this );

    getInitialState(): Object {
        return {
            ...super.getInitialState()
        }
    }

    _onChange(state): void {
        // As soon as we retrieve the readed messages from local storage
        if(state.commonsStore_readedMessages){
            state.unreadedMessages = _.differenceBy(this.state.commonsStore_bootstrap.messages, state.commonsStore_readedMessages, 'id');
            // If there's no messages to read, keep going
            if(!state.unreadedMessages.length) state.commonsStore_isUnreadedReaded = true;
        }

        super._onChange(state);
    }
    
    componentDidUpdate(prevProps, prevState){
        if(this.state.unreadedMessages.length && this.state.commonsStore_spinner) {
            return super.getCommonsActions().spinner(false);
        }

        if(this.state.commonsStore_isUnreadedReaded){
            return super.getAuthActions().authWithStoredToken();
        }

        return super.componentDidUpdate(prevProps, prevState);
    }

    _handleReadMessages(): void {
        super.getCommonsActions().readMessages(this.state.unreadedMessages);
    }
    
    componentDidMount(): void {
        super.componentDidMount();
        super.getCommonsActions().getReadedMessages();
    }

    render(): any {
        return (
            <Container style={ Style.get(' background.splash_primary ') }>
                <Content>
                    <View style={Style.get('flex')}>
                        <Image
                            source={ require('../assets/img/splash-logo.png') }
                            resizeMode='contain'
                            style={ Style.get(' img_large push_center margin.bottom._5 ') }
                        />
                    </View>
                    { this.state.unreadedMessages ? (
                        <View style={Style.get('flex padding._30')}>
                            {this.state.unreadedMessages.map((m, i) => {
                                return (
                                    <Text key={i} style={Style.get('color.white')}>{m.message}</Text>
                                )
                            })}
                        </View>
                    ) : null}
                    <View>
                        <TouchableOpacity onPress={this._handleReadMessages} style={Style.get('padding._10 background.white alignSelf.center')}>
                            <Text style={Style.get('color.dark_primary text_center')}>Ok</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        );
    }
}