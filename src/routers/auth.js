/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import * as React from "react";
import _ from 'lodash';
import { Image, View } from "react-native";
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Header, Left, Label, Item, Form, Input, Text, Button, Icon, Body, Title, Right, Content } from "native-base";
import {FBLogin, FBLoginManager} from 'react-native-facebook-login';

import Container from '../components/container';

import ScreenComponent from '../components/screen';
import Style from '../services/styles';
import Config from '../services/config';
import Utils from '../services/utils';
import i18n from '../services/i18n';

export default class AuthScreen extends ScreenComponent {

    static navigationOptions = {
        drawerLockMode: 'locked-closed',
    };

    _handleSignin: any;
    state: Object = this.getInitialState();
    
    constructor( props: any ){
        super( props );
        this._handleSignin = this._handleSignin.bind( this );
    }

    getInitialState(): Object{
        return {
            ...super.getInitialState(),
            signin_email: null,
            signin_password: null
        }
    }

    componentDidMount(): void {
        super.componentDidMount();
        super.getCommonsActions().spinner(false);
    }

    componentWillUnmount(): void{
        super.componentWillUnmount()
    }

    async _handleSignin(): Promise<void>{
        try{
            
            await Utils.validate( 'email', this.state.signin_email );
            await Utils.validate( 'password', this.state.signin_password );

            super.getAuthActions().fetchToken({
                email: this.state.signin_email,
                password: this.state.signin_password
            });
        }
        catch( ERR ){
            super.getCommonsActions().throwError( ERR );
        }
    }

    render(): any {
        return (
            <Container style={ Style.get(' background.splash_primary ') }>
                <Content contentContainerStyle={ Style.get(' content.centered ') }>
                    <Grid style={ Style.get(' grid.centered ') }>
                        <Col>
                            <Image
                                source={ require('../assets/img/splash-logo.png') }
                                resizeMode='contain'
                                style={ Style.get(' img_large push_center margin.bottom._30 ') }
                            />
                            {Config.get('AUTH.ENABLE_LOCAL_AUTH') ? (
                                <View>
                                    <Item
                                        regular
                                        style={ Style.get(' margin.left._0 push_center background.white ') }>
                                        <Input
                                            keyboardType='email-address'
                                            onChangeText={ ( v ) => this.setState({signin_email: v })}
                                            value={ this.state.signin_email }
                                            style={ Style.get(' color.dark_primary border.none ') }
                                            placeholder={ i18n.__('label.email') }
                                            />
                                    </Item>
                                    <Item regular
                                        style={ Style.get(' margin.left._0 push_center background.white ') }>
                                        <Input 
                                            secureTextEntry={ true }
                                            onChangeText={ ( v ) => this.setState({signin_password: v })}
                                            value={ this.state.signin_password }
                                            style={ Style.get(' color.dark_primary border.none ') }
                                            placeholder={ i18n.__('label.password') }
                                        />
                                    </Item>
                                    <Button 
                                        onPress={ this._handleSignin }
                                        block
                                        style={ Style.get(' background.accent border.square ') }>
                                        <Text style={ Style.get(' color.white ') }>
                                            { i18n.__('login').toUpperCase() }
                                        </Text>
                                    </Button>
                                </View>
                            ) : null}
                            {Config.get('AUTH.ENABLE_FACEBOOK_SIGNIN') ? (
                                <FBLogin
                                    style={{height: 250}}
                                    ref={(fbLogin) => { this.fbLogin = fbLogin }}
                                    loginBehavior={FBLoginManager.LoginBehaviors.Native}
                                    permissions={["email","user_friends"]}
                                    onLogin={function(e){console.log(e)}}
                                    onLoginFound={function(e){console.log(e)}}
                                    onLoginNotFound={function(e){console.log(e)}}
                                    onLogout={function(e){console.log(e)}}
                                    onCancel={function(e){console.log(e)}}
                                    onPermissionsMissing={function(e){console.log(e)}}
                                />
                            ) : null}
                            {Config.get('AUTH.ENABLE_LOCAL_AUTH') ? (
                                <Button 
                                    onPress={ () => { this.props.navigation.navigate( 'Signup' )  } }
                                    transparent
                                    style={ Style.get(' margin.top._30 push_center border.square ') }>
                                    <Text style={ Style.get(' font.small color.white ') }>
                                        { i18n.__('signup').toUpperCase() }
                                    </Text>
                                </Button>
                            ) : null}
                        </Col>
                    </Grid>
                </Content>
            </Container>
        );
    }
}