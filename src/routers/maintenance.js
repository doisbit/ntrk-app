/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */
import * as React from "react";
import _ from 'lodash';
import { View, Image } from 'react-native';
import { Header, Left, Text, Button, Icon, Body, Title, Right, Content } from "native-base";

import Container from '../components/container';
import ScreenComponent from '../components/screen';
import Style from '../services/styles';
import i18n from '../services/i18n';

export default class MaintenanceScreen extends ScreenComponent {

    static navigationOptions = {
        drawerLockMode: 'locked-closed',
    };

    componentDidMount(): void {
        super.componentDidMount();
        super.getCommonsActions().spinner(false);
    }

    render(): any {
        return (
            <Container style={ Style.get(' background.splash_primary ') }>
                <Content contentContainerStyle={ Style.get(' content.centered ') }>
                    <Image
                        source={ require('../assets/img/splash-logo.png') }
                        resizeMode='contain'
                        style={ Style.get(' img_large push_center margin.bottom._30 ') }
                    />
                    <Text style={{ alignSelf: 'center', ...Style.get( 'color.white text_center' ) }}>
                        {i18n.__('messages.maintenance')}
                    </Text>
                </Content>
            </Container>
        );
    }
}