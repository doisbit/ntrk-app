/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import React from 'react';
import { Image } from 'react-native';

import { Content } from 'native-base';

import ScreenComponent from '../components/screen';
import Container from '../components/container';

import Style from '../services/styles';

import Config from '../services/config';

export default class Splash extends ScreenComponent {

    static navigationOptions = {
        drawerLockMode: 'locked-closed',
    };

    state: any = {
        ...super.getInitialState(),
        isLoaded: false
    }

    componentDidMount(): void {
        super.componentDidMount();
        // In order to "simulate" a splash screen, we should
        // just do something after 2s. :)
        setTimeout(() => {
            // Checks if there is already an auth JWT token stored into database
            super.getCommonsActions().getBootstrap();
            this.setState( _.assign( {}, { isLoaded: true } ) );
            super.getCommonsActions().spinner(true);
        }, 2000);
    }

    componentWillUpdate(props, state){
        // The app has been loaded
        if(state.commonsStore_bootstrap) {
            // If maintenance mode is on. Redirect to maintenance screen
            if( state.commonsStore_bootstrap.options.maintenance == 'on' ){
                return setImmediate(this.props.navigation.navigate.bind(
                    this, 'Maintenance'
                ))
            }
            else if( state.commonsStore_bootstrap.messages ){
                return setImmediate(this.props.navigation.navigate.bind(
                    this, 'Messages'
                ))
            }
            // Otherwise. If app is loaded, keep flux
            else if( state.isLoaded ){
                super.getAuthActions().authWithStoredToken();
            }
        }
    }

    render() {
        return (
            <Container style={ Style.get(' background.splash_primary ') }>
                <Content contentContainerStyle={ Style.get(' content.centered ') }>
                    <Image
                        source={ require('../assets/img/splash-logo.png') }
                        resizeMode='contain'
                        style={ Style.get(' img_large push_center margin.bottom._30 ') }
                    />
                </Content>
            </Container>
        );
    }
}
