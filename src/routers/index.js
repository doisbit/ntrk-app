/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import React, { Component } from "react";
import { DrawerNavigator } from "react-navigation";
import { Text } from 'react-native';

import AuthScreen from "./auth.js";
import LogoutScreen from "./logout.js";
import SignupScreen from "./signup.js";
import SplashScreen from "./splash.js";
import MaintenanceScreen from "./maintenance.js";
import MessagesScreen from "./messages.js";

import SideBar from "../components/SideBar/component.js";

import NowTrack from '../plugins/nowtrack/nowtrack';

const AppRouter = DrawerNavigator(
    {
        Splash: { screen: SplashScreen },
        Auth: { screen: AuthScreen },
        Signup: { screen: SignupScreen },
        Logout: { screen: LogoutScreen },
        Maintenance: { screen: MaintenanceScreen },
        Messages: { screen: MessagesScreen },
        ...NowTrack.setupRoutes(),
    },
    {
        drawerOpenRoute: 'DrawerOpen',
        drawerCloseRoute: 'DrawerClose',
        drawerToggleRoute: 'DrawerToggle',
        contentComponent: props => <SideBar { ...props } />
    }
)

export default AppRouter;