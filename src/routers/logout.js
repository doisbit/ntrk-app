/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */
import * as React from "react";
import _ from 'lodash';
import { View } from 'react-native';
import { Header, Left, Text, Button, Icon, Body, Title, Right, Content } from "native-base";

import Container from '../components/container';
import ScreenComponent from '../components/screen';
import Style from '../services/styles';

export default class LogoutScreen extends ScreenComponent {

    static navigationOptions = {
        drawerLockMode: 'locked-closed',
    };

    componentDidMount(): void {
        super.componentDidMount();
        super.getAuthActions().logout();
    }

    render(): any {
        return (
            <View style={ Style.get(' flex ') }>
                <Text>Carregando...</Text>
            </View>
        );
    }
}