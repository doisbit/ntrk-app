/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Alt from '../vendors/alt';
import SQLite from 'react-native-sqlite-storage';
import _ from 'lodash';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import Config from '../services/config';
import Api from '../services/api';
import * as React from "react";

type Message = {
    id: string,
    message: string
}

/**
 * CommonsActions
 * 
 * Class containing actions related to common stuff
 * 
 * @author Alexandre Moraes
 */
class CommonsActions {

    db: any;

    constructor() {
        this.db = SQLite.openDatabase(Config.get("DB.NAME"), "1.0", "Demo", -1);
        this.bootstrap();
    }

    bootstrap() {
        this.db.transaction(tx => {
            // Table responsible to store readed messages from administrator
            tx.executeSql( 'CREATE TABLE IF NOT EXISTS messages (id integer );');
        });
    }

    /**
     * Shall we show the spinner
     * 
     * @param { boolean } state The spinner state
     */
    spinner(state: boolean): boolean {
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( state );
                })
            }
            else {
                dispatch( state );
            }
        }
    }

    /**
     * Retrieves the readed messages from the local storage
     */
    getReadedMessages(): void {
        return (dispatch) => {
            try{
                this.db.transaction(tx => {
                    tx.executeSql( 'SELECT * FROM messages', [], (tx, results) => {
                        let messages = [];
                        let done = false;
                        if(results.rows.length){
                            done = _.after(results.rows.length, () => {
                                return this.setReadedMessages(messages);
                            });
                            for(let i = 0; i < results.rows.length; i++){
                                messages.push(results.rows.item(i));
                                done();
                            }
                        }
                        else{
                            return this.setReadedMessages(messages);
                        }
                    })
                });
            }
            catch( ERR ){
                return this.throwError( ERR );
            }
        }
    }

    /**
     * Set the already readed messages into state
     * @param {Array<Message>} messages 
     */
    setReadedMessages(messages): Array<Message> {
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( messages );
                })
            }
            else {
                dispatch( messages );
            }
        }
    }

    /**
     * Stores the given array of messages into the readed messages table
     * 
     * @param {Array<Message>} messages 
     */
    readMessages(messages) {
        return (dispatch) => {
            try{
                _.each(messages, m => {
                    this.db.transaction(tx => {
                        tx.executeSql('INSERT INTO messages (id) VALUES (?);', [m.id]);
                    });
                });
                if(Alt.dispatcher.isDispatching()){
                    setTimeout(() => {
                        dispatch(true);
                    })
                }
                else {
                    dispatch(true);
                }
            }
            catch( ERR ){
                return this.throwError( ERR );
            }
        }
    }

    /**
     * Get the app bootstrap data
     */
    getBootstrap() {
        return async ( dispatch ) => {
            try {
                let response = await Api.get( Config.get("API.INFO") );
                if ( !response.status ) return this.throwError( response );
                return this.setBootstrap( response.data );
            }
            catch ( ERR ) {
                return this.throwError( ERR );
            }
        };
    }

    setBootstrap( bootstrap: Object ): Object {
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( bootstrap );
                })
            }
            else {
                dispatch( bootstrap );
            }
        }
    }

    /**
     * Throw Error
     * 
     * Called when something goes wrong
     * 
     * @param { any } error The error to throw
     * @returns { any } The error to throw
     */
    throwError(error:any) {
        return (dispatch) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( error );
                })
            }
            else {
                dispatch( error );
            }
        }
    }

}

export default Alt.createActions(CommonsActions);