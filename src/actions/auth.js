/*
 * 2017 Doisbit.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @flow
 */

import Alt from '../vendors/alt';
import SQLite from 'react-native-sqlite-storage';
import Config from '../services/config';
import Api from '../services/api';

import {FBLogin, FBLoginManager} from 'react-native-facebook-login';

import CommonsActions from './commons';

import * as React from "react";

/**
 * AuthActions
 * 
 * Class containing actions related to authentication
 * 
 * @author Alexandre Moraes
 */
class AuthActions {

    constructor() {
        this.db = SQLite.openDatabase(Config.get("DB.NAME"), "1.0", "Demo", -1);
        this.bootstrap();
    }

    /**
     * Bootstrap
     * 
     * Should be called as the first thing on the first screen.
     * 
     * Loads the specific databases from this specific 
     */
    bootstrap() {
        this.db.transaction(tx => {
            // Table responsible to store JWT authentication tokens in order to prevent user
            // from login every time he opens the app
            tx.executeSql( 'CREATE TABLE IF NOT EXISTS auth (token text );');
        });
    }

    /**
     * Fetch Facebook Login
     * 
     * Called when an user clicks on a "login with facebook" button
     * 
     * @triggers this.setToken
     */
    fetchFacebookLogin() {
        return async ( dispatch ) => {
            // dispatch();
            try {
                /*const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync(
                    Config.get("FB.CLIENT_ID"), { permissions: Config.get("FB.AUTH_PERMISSIONS") }
                );
                switch ( type ) {
                    case 'success': {
                        const response = await fetch(`https://graph.facebook.com/me?access_token=${ token }`);
                        const profile = await response.json();
                        profile.provider = "facebook";
                        const JWTToken = await this.oauthLogin({ profile: profile, token: token });
                        return this.setToken( JWTToken );
                        break;
                    }
                    case 'cancel': {
                        return CommonsActions.throwError('OAUTH_REVOKE');
                        break;
                    }
                    default: {
                        return CommonsActions.throwError('AUTH_ERROR');
                    }
                }*/
            } catch ( ERR ) {
                return CommonsActions.throwError( ERR );
            }
        }
    }

    authWithStoredToken(){
        return ( dispatch ) => {
            try{
                this.db.transaction(tx => {
                    tx.executeSql( 'SELECT * FROM auth', [], (tx, results) => {
                        if( results.rows.length ) {
                            Api.setJWT( results.rows.item(0).token );
                            return this.refreshToken( results.rows.item(0).token );
                        }
                        return this.updateLocalAuthStatus( false );
                    } )
                });
            }
            catch( ERR ){
                return CommonsActions.throwError( ERR );
            }
        }
    }

    /**
     * Triggered when it looks for a stored authentication token.
     * 
     * @param { boolean } status If there is a stored auth on the database
     */
    updateLocalAuthStatus( status ){
        return ( dispatch ) => {
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( status );
                })
            }
            else {
                dispatch( status );
            }
        }
    }

    /**
     * OAuth Login
     * 
     * @param { Object } body The OAuth data to authenticate
     * 
     * @triggers this.setToken
     */
    oauthLogin( body ) {
        return new Promise( async ( resolve, reject ) => {
            try {
                let response = await Api.post( Config.get("API.OAUTH.LOGIN"), body );
                if ( !response.status ) reject( response.message );
                resolve( response.data );
            }
            catch ( ERR ) {
                reject( ERR.message );
            }
        } );
    }

    /**
     * Fetch Signup
     * 
     * Called when an user signup via local authentication
     * 
     * @param { Object } body The user data to authenticate
     * 
     * @triggers this.setToken
     */
    async fetchSignup( body ) {
        try {
            let response = await Api.post( Config.get("API.AUTH.SIGNUP"), body );
            if ( !response.status ) return CommonsActions.throwError( response );
            return this.setToken( response.data );
        }
        catch ( ERR ) {
            return CommonsActions.throwError( ERR );
        }
    }

    /**
     * Refresh Token
     * 
     * Called when a user wants a refreshed JWT token
     * 
     * @param {String} jwt The user JWT token to refresh
     * 
     * @triggers this.setToken
     */
    async refreshToken( jwt ) {
        try{
            let response = await Api.get( Config.get("API.AUTH.REFRESH_TOKEN") );
            if ( !response.status ) return CommonsActions.throwError( response );
            return this.setToken( response.data );
        }
        catch( ERR ){
            return CommonsActions.throwError( ERR );
        }
    }

    /**
     * Fetch Token
     * 
     * Called when an user signin via local authentication
     * 
     * @param { Object } body The user data to authenticate
     * 
     * @triggers this.setToken
     */
    fetchToken( body ) {
        return async ( dispatch ) => {
            try{
                let response = await Api.post( Config.get("API.AUTH.TOKEN"), body );
                if ( !response.status ) return CommonsActions.throwError( response );
                return this.setToken( response.data );
            }
            catch( ERR ){
                return CommonsActions.throwError( ERR );
            }
        }
    }

    logout(){
        return ( dispatch ) => {
            Api.dropJWT();
            this.db.transaction(tx => {
                tx.executeSql('DELETE FROM auth;');
            }, null, () => {
                if(Alt.dispatcher.isDispatching()){
                    setTimeout(() => {
                        dispatch();
                    })
                }
                else {
                    dispatch();
                }
            });
        };
    }

    /**
     * Set Token
     * 
     * Called when an authentication on API succeeds.
     * 
     * Stores the JWT token
     * 
     * @param { string } token The JWT Token
     * @param { boolean } fromDb If false, it will save the token in the database
     * @returns { string } The authenticated token
     */
    setToken( token, fromDb: Boolean = false ) {
        return ( dispatch ) => {
            if( !fromDb ){
                this.db.transaction(tx => {
                    tx.executeSql('DELETE FROM auth;');
                    if( token ) tx.executeSql('INSERT INTO auth (token) VALUES (?);', [token]);
                });
            }
            if(Alt.dispatcher.isDispatching()){
                setTimeout(() => {
                    dispatch( { token: token, fromDb: fromDb } );
                })
            }
            else {
                dispatch( { token: token, fromDb: fromDb } );
            }
        }
    }

}

export default Alt.createActions( AuthActions );