/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';

import moment from 'moment';

import {
    Platform,
    StyleSheet,
    Text,
    StatusBar,
    View,
    NativeModules
} from 'react-native';

import { Root } from "native-base";

import { Sentry } from 'react-native-sentry';

import MainRouter from "./src/routers/index.js";

import Style from './src/services/styles';
import Config from './src/services/config';

const sentryDsn = Platform.select({
    "ios": Config.get('SENTRY.IOS'),
    "android": Config.get('SENTRY.ANDROID')
});

Sentry.config(sentryDsn).install();

export default class App extends React.Component {

    render() {
        return (
            <View style={{ ...Style.get('flex') }}>
                <Root>
                    <MainRouter />
                </Root>
            </View>
        );
    }
}